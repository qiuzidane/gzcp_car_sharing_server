package com.icbc.itsp.carsharing.service.serviceImpl;

import com.icbc.itsp.carsharing.dao.AddressDao;
import com.icbc.itsp.carsharing.entity.Address;
import com.icbc.itsp.carsharing.response.Result;
import com.icbc.itsp.carsharing.service.AddressService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {
    @Resource
    private AddressDao addressDao;
    @Override
    public List<Address> queryAddress(Address address){
        List<Address> list = addressDao.queryAddress(address);
        return list;
    }


    @Override
    public Result addAddress(Address address){
        Integer i = addressDao.insertAddress(address);
        if(i > 0){
            return Result.success("新增地址成功！");
        }else {
            return Result.fail("新增地址失败！");
        }
    }

    @Override
    public Integer deleteAddress(Address address){
        Integer i = addressDao.deleteByPrimaryKeySelective(address);
        return i;
    }

    @Override
    public Result updateAddress(Address address){
        Integer i = addressDao.updateByUniqueKeySelective(address);
        if(i > 0){
            return Result.success("修改地址成功！");
        }else {
            return Result.fail("修改地址失败！");
        }
    }

}

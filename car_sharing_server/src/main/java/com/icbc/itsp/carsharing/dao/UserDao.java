package com.icbc.itsp.carsharing.dao;

import com.icbc.itsp.carsharing.entity.QueryParamVo;
import com.icbc.itsp.carsharing.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {
    List<User> queryUser(QueryParamVo<User> queryParamVo);
    Integer insertUser(User user);
    Integer deleteByPrimaryKeySelective(User user);
    Integer updateByPrimaryKeySelective(User user);
    Integer updateByUniqueKeySelective(User user);
    User queryUserByUniqueKey(@Param("openid") String openid);
    User queryUserByPrimaryKey(@Param("id") Integer id);
    Integer queryUserCount(QueryParamVo<User> queryParamVo);
}

package com.icbc.itsp.carsharing.enumType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum CarpoolDtlStatus {
    /**
     * 1.乘客申请拼车，初始状态为[待车主同意]
     * 2.司机审批不通过，状态为[拼车失败]
     * 3.司机审批通过，状态为[拼车完成]
     * 4.司机取消拼车总单，状态为[司机已取消]
     * 5.乘客点取消拼车，状态为[乘客已取消]
     */
    PENDING("待车主同意"),
    FAILED("拼车失败"),
    FINISHED("拼车完成"),
    CANCEL_BY_DRIVER("司机已取消"),
    CANCEL_BY_PASSENGER("乘客已取消");
    private String status;
}

package com.icbc.itsp.carsharing.service;

import com.icbc.itsp.carsharing.entity.PassengerInfo;
import java.util.List;

public interface PassengerInfoService {

    /**
     * 根据openid查询单条记录
     * @param openid
     * @return
     */
    PassengerInfo queryPassengerInfoByUniqueKey(String openid);
    /**
     * 多条查询
     * @return
     */
    List<PassengerInfo> queryPassengerInfo(PassengerInfo passengerInfo);

    /**
     * 新增
     * @param passengerInfo
     * @return
     */
    boolean addPassengerInfo(PassengerInfo passengerInfo);

    /**
     * 更新
     * @param passengerInfo
     * @return
     */
    boolean updatePassengerInfo(PassengerInfo passengerInfo);
}

package com.icbc.itsp.carsharing.dao;

import com.icbc.itsp.carsharing.entity.CarpoolDtl;
import com.icbc.itsp.carsharing.entity.User;
import org.apache.ibatis.annotations.Param;

/**
 * Created by Big_Cat on 2020/2/7 10:13
 */
public interface CommonDao {
    /**
     * 查询拼车剩余可选座位数
     */
    Integer getCarpoolRemainSeat(@Param(value = "carpoolId") Integer carpoolId);

    /**
     * 查询历史拼车剩余可选座位
     * @param orgCarpoolId
     * @return
     */
    Integer getCarpoolRemainSeatHistory(@Param(value = "orgCarpoolId") Integer orgCarpoolId);

    /**
     * 根据乘客id查询用户微信头像地址
     */
    String queryAvatarUrlByPassengerId(@Param(value = "passengerId") Integer passengerId);

    /**
     * 根据乘客id查询用户部门
     */
    String queryDepartmentByPassengerId(@Param(value = "passengerId") Integer passengerId);


    /**
     * 根据乘客id查询用户微信头像地址
     */
    String queryAvatarUrlByDriverId(@Param(value = "driverId") Integer driverId);

    /**
     * 根据乘客id查询用户部门
     */
    String queryDepartmentByDriverId(@Param(value = "driverId") Integer driverId);

    /**
     * 根据乘客id查询用户信息
     */
    User queryUserByPassengerId(@Param(value = "passengerId") Integer passengerId);

    /**
     * 根据车主id查询用户信息
     */
    User queryUserByDriverId(@Param(value = "driverId") Integer driverId);
}

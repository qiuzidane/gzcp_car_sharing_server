package com.icbc.itsp.carsharing.enumType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum EmployeeFlag {
    //0-员工 ； 1-非员工 ；2-待审批
    IS_EMPLOYEE("0"),
    NOT_EMPLOYEE("1"),
    PENDING("2");
    private String flag;
}

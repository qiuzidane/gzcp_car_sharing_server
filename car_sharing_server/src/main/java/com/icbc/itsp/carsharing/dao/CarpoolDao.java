package com.icbc.itsp.carsharing.dao;

import com.icbc.itsp.carsharing.entity.Carpool;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CarpoolDao {
    List<Carpool> queryCarpool(Carpool carpool);

    Carpool queryByPrimaryKey(@Param("id") Integer id);

    Integer insertCarpool(Carpool carpool);

    Integer updateByPrimaryKeySelective(Carpool carpool);

    List<Carpool> findCarpoolByIds(Map<String, Object> idList);

    Carpool queryByParam(Carpool carpool);

    void deleteByParam(Carpool carpool);
}

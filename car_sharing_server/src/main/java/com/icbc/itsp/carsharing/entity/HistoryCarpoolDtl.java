package com.icbc.itsp.carsharing.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.persistence.Transient;
import java.util.Date;

/**
 * 拼车历史明细表
 */
@Data
public class HistoryCarpoolDtl {
    private Integer id;
    private Integer passengerId;//乘客个人信息表id
    private Integer carpoolId; //总表id
    private String passengerName;  // 乘客名称
    private String phoneNumber;   // 乘客电话
    private Integer startLocation;     // 乘客上车地点 -地址信息保存到地址表，保存地址id
    private Integer endLocation;     // 乘客下车地点 -地址信息保存到地址表，保存地址id
    private String status; //拼车状态
    private String remark;  //原因
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime; //申请时间
    @Transient
    private Address carStartLocationAddress;
    @Transient
    private Address startLocationAddress;
    @Transient
    private Address endLocationAddress;
    @Transient
    private User user;
}

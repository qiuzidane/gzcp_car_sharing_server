package com.icbc.itsp.carsharing.dao;

import com.icbc.itsp.carsharing.entity.DriverInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DriverInfoDao {
    /**
     * 查询所有司机发布信息
     * @return
     */
    List<DriverInfo> queryDriverInfo(DriverInfo driverInfo);

    DriverInfo queryDriverInfoByUniqueKey(@Param("openid") String openid);
    DriverInfo queryDriverInfoByPrimaryKey(@Param("id") Integer id);

    /**
     * 新增车主个人信息
     * @param driverInfo
     * @return
     */
    boolean insertDriverInfo(DriverInfo driverInfo);

    /**
     * 更新车主个人信息
     * @param driverInfo
     * @return
     */
    boolean updateDriverInfo(DriverInfo driverInfo);
}

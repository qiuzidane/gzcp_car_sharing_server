package com.icbc.itsp.carsharing.controller;

import com.icbc.itsp.carsharing.entity.HistoryCarpool;
import com.icbc.itsp.carsharing.entity.HistoryCarpoolDtl;
import com.icbc.itsp.carsharing.response.Result;
import com.icbc.itsp.carsharing.service.HistoryCarpoolDtlService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/historyCarpoolDtl")
@CrossOrigin
@Slf4j
public class HistoryCarpoolDtlController {
    @Resource
    private HistoryCarpoolDtlService historyCarpoolDtlService;

    /**
     * 查询拼车历史明细记录
     * @param historyCarpoolDtl
     * @return
     */
    @RequestMapping(value = "/query",method = RequestMethod.GET)
    public Result queryHistoryCarpoolDtl(HistoryCarpoolDtl historyCarpoolDtl){
        List<HistoryCarpoolDtl> list = historyCarpoolDtlService.queryHistoryCarpoolDtl(historyCarpoolDtl);
        return Result.success(list);
    }

    /**
     * 根据乘客id获取拼车历史总表记录
     * @param historyCarpoolDtl
     * @return
     */
    @RequestMapping(value = "/queryCarpoolByPassengerId",method = RequestMethod.GET)
    public Result queryCarpoolByPassengerId(HistoryCarpoolDtl historyCarpoolDtl){
        try {
            List<HistoryCarpool> list = historyCarpoolDtlService.queryCarpoolByPassengerId(historyCarpoolDtl);
            return Result.success(list);
        }catch (Exception e){
            e.printStackTrace();
            return Result.fail("查询历史拼车明细总表记录发生异常！");
        }
    }
}

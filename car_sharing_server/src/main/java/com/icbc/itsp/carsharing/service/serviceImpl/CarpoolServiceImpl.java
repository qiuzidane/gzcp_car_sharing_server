package com.icbc.itsp.carsharing.service.serviceImpl;

import com.icbc.itsp.carsharing.dao.*;
import com.icbc.itsp.carsharing.entity.*;
import com.icbc.itsp.carsharing.enumType.CarpoolDtlStatus;
import com.icbc.itsp.carsharing.enumType.CarpoolStatus;
import com.icbc.itsp.carsharing.response.Result;
import com.icbc.itsp.carsharing.service.CarpoolService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class CarpoolServiceImpl implements CarpoolService {
    @Resource
    private CarpoolDao carpoolDao;
    @Resource
    private CarpoolDtlDao carpoolDtlDao;
    @Resource
    private HistoryCarpoolDao historyCarpoolDao;
    @Resource
    private HistoryCarpoolDtlDao historyCarpoolDtlDao;
    @Resource
	private CommonDao commonDao;
    @Resource
	private AddressDao addressDao;
    @Resource
	private DriverInfoDao driverInfoDao;
    @Resource
	private UserDao userDao ;
    
    @Override
    public List<Carpool> queryCarpool(Carpool carpool){
//		carpool.setStatus(CarpoolStatus.UNFINISHED.getStatus());
//		carpool.setStartTime(new Date());
        List<Carpool> list = carpoolDao.queryCarpool(carpool);
        for (Carpool carpooltemp : list){
            //拼车明细列表
            CarpoolDtl carpoolDtl = new CarpoolDtl();
            carpoolDtl.setCarpoolId(carpooltemp.getId());
            List<CarpoolDtl> carpoolDtlList = carpoolDtlDao.queryCarpoolDtl(carpoolDtl);
            carpooltemp.setCarpoolDtlList(carpoolDtlList);
            //发车地点对象
        	if(carpooltemp.getStartLocation() != null){
				Address startAddress = addressDao.queryAddressByPrimaryKey(carpooltemp.getStartLocation());
				carpooltemp.setStartLocationAddress(startAddress);
			}
        	//终点对象
			if(carpooltemp.getEndLocation() != null){
				Address endAddress = addressDao.queryAddressByPrimaryKey(carpooltemp.getEndLocation());
				carpooltemp.setEndLocationAddress(endAddress);
			}
			//用户对象
			if(carpooltemp.getDriverId() != null){
				User user = commonDao.queryUserByDriverId(carpooltemp.getDriverId());
				carpooltemp.setUser(user);
			}
			//设置已拼车人数
			Integer remainSeat = commonDao.getCarpoolRemainSeat(carpooltemp.getId()) == null ? 0 : commonDao.getCarpoolRemainSeat(carpooltemp.getId());
			Integer totalSeat = carpooltemp.getTotalSeat() == null ? 0 : carpooltemp.getTotalSeat();
			Integer carpoolSuccessSeat = totalSeat - remainSeat;
			carpooltemp.setCarpoolSuccessSeat(carpoolSuccessSeat > 0 ? carpoolSuccessSeat : 0);

		}
        return list;
    }
    
    /**
     *1、取消未结束的拼车
     * 2、完成拼车
     * 处理拼车记录
     */
	@Override
	public boolean deleteCarpool(Carpool carpoolParam) {
		try{
			// 1、根据id查拼车总单和拼车明细单
			Carpool carpoolTemp = carpoolDao.queryByParam(carpoolParam);
			CarpoolDtl carpoolDtl = new CarpoolDtl();
			// 查出所有明细单
			carpoolDtl.setCarpoolId(carpoolParam.getId());
			List<CarpoolDtl> list = carpoolDtlDao.queryCarpoolDtl(carpoolDtl);
			if(list == null) return false;
			// 2、复制总单
			HistoryCarpool historyCarpool = new HistoryCarpool();
			BeanUtils.copyProperties(carpoolTemp, historyCarpool);
			historyCarpool.setOrgCarpoolId(carpoolTemp.getId());
			if(carpoolParam.getStatus().equals("取消")){
				//  取消拼车---订单已完成能：座位满了就已经是拼车完成了，可以取消
				historyCarpool.setStatus(CarpoolStatus.CANCEL.getStatus());
				for (CarpoolDtl carpoolDtlTemp : list) {
					HistoryCarpoolDtl historyCarpoolDtl = new HistoryCarpoolDtl();
					BeanUtils.copyProperties(carpoolDtlTemp, historyCarpoolDtl);
					// 待同意的和完成的 改为司机取消
					if(historyCarpoolDtl.getStatus().equals(CarpoolDtlStatus.PENDING.getStatus()) || 
							historyCarpoolDtl.getStatus().equals(CarpoolDtlStatus.FINISHED.getStatus())){
						historyCarpoolDtl.setStatus(CarpoolDtlStatus.CANCEL_BY_DRIVER.getStatus());
					}
					historyCarpoolDtl.setCarpoolId(carpoolTemp.getId());
					historyCarpoolDtl.setRemark(carpoolParam.getRemark());
					historyCarpoolDtlDao.insert(historyCarpoolDtl);
				}
			}else{
				// 完成拼车
				historyCarpool.setStatus(CarpoolStatus.FINISHED.getStatus());
				for (CarpoolDtl carpoolDtlTemp : list) {
					HistoryCarpoolDtl historyCarpoolDtl = new HistoryCarpoolDtl();
					BeanUtils.copyProperties(carpoolDtlTemp, historyCarpoolDtl);
					historyCarpoolDtl.setCarpoolId(carpoolTemp.getId());
					historyCarpoolDtlDao.insert(historyCarpoolDtl);
				}
			}
			historyCarpoolDao.insert(historyCarpool);

			// 3、再根据拼车总单删除拼车明细记录和拼车总单记录。
			carpoolDao.deleteByParam(carpoolParam);
			carpoolDtlDao.deleteCarpoolDtl(carpoolDtl);
			return true;
		}catch (Exception e) {
			log.error("处理拼车总单和明细异常。", e);
		}
		return false;
	}

	@Override
	public List<Carpool> findCarpoolByIds(Map<String, Object> idList) {
		return carpoolDao.findCarpoolByIds(idList);
	}

	@Override
    public Result releaseCarpool(Carpool carpool){
		Result result = checkDriverInfo(carpool);
		if(!result.isSuccess()){
			return result;
		}
		DriverInfo driverInfo = driverInfoDao.queryDriverInfoByPrimaryKey(carpool.getDriverId());
		Carpool carpoolParam = new Carpool();
		carpoolParam.setDriverId(carpool.getDriverId());
//		carpoolParam.setStatus(CarpoolStatus.UNFINISHED.getStatus());
		List<Carpool> list = carpoolDao.queryCarpool(carpoolParam);
		if(list.size() > 0){
			return Result.fail("您有未结束的拼车，不能继续新增！");
		}
		if(driverInfo.getTotalSeat() == null || driverInfo.getTotalSeat() <= 0){
			return Result.fail("总共可拼座位数必须大于0！");
		}else{
			carpool.setTotalSeat(driverInfo.getTotalSeat());
		}
		if(carpool.getStartLocation() == null){
			return Result.fail("发车地点不能为空！");
		}
		if(carpool.getEndLocation() == null){
			return Result.fail("终点不能为空！");
		}
		carpool.setCarNo(driverInfo.getCarNo());
		carpool.setTotalSeat(driverInfo.getTotalSeat());
		carpool.setStatus(CarpoolStatus.UNFINISHED.getStatus());
		carpool.setCreateTime(new Date());
        Integer i = carpoolDao.insertCarpool(carpool);
		if(i > 0){
			return Result.success("发布拼车成功！");
		}else {
			return Result.fail("发布拼车失败！");
		}
    }

    @Override
    public Result update(Carpool carpool){
		Result result = checkDriverInfo(carpool);
		if(!result.isSuccess()){
			return result;
		}
		DriverInfo driverInfo = driverInfoDao.queryDriverInfoByPrimaryKey(carpool.getDriverId());
		Carpool loadCarpool = carpoolDao.queryByPrimaryKey(carpool.getId());
		//修改前的总共可拼乘客数量
		int oldTotalSeat = loadCarpool.getTotalSeat() == null ? 0 : loadCarpool.getTotalSeat();
		//修改前剩余可拼座位数量
		int remainSeat = commonDao.getCarpoolRemainSeat(carpool.getId()) == null ? 0 : commonDao.getCarpoolRemainSeat(carpool.getId());
		//已经审批通过的拼车乘客数量
		int carpoolSucessQty = oldTotalSeat - remainSeat;

		//修改后的总共可拼乘客数量
		int newTotalSeat = carpool.getTotalSeat();
		if(carpoolSucessQty > newTotalSeat){
			return Result.fail("修改后的总共可拼乘客人数小于已审批通过的拼车人数，更新失败！如需修改可拼座位数，请取消当前拼车单，从新发布拼车！");
		}else if(carpoolSucessQty == newTotalSeat){
			//如果修改总共可拼乘客数量后，剩余位置为0，则更新拼车总表记录的状态为完成拼车
			carpool.setStatus(CarpoolStatus.FINISHED.getStatus());
		}
		carpool.setCarNo(driverInfo.getCarNo());
		carpool.setTotalSeat(driverInfo.getTotalSeat());
		//默认：公司
		if(carpool.getStartLocation() == null){
			carpool.setStartLocation(driverInfo.getAfterWorkStartLocation());
		}
		if(carpool.getEndLocation() == null){
			carpool.setEndLocation(driverInfo.getAfterWorkStartLocation());
		}
		Integer i = carpoolDao.updateByPrimaryKeySelective(carpool);
		if(i > 0){
			return Result.success("更新成功！");
		}else {
			return Result.fail("更新失败！");
		}
	}

	/**
	 * 检查车主信息
	 * @return
	 */
	private Result checkDriverInfo(Carpool carpool){
		if(carpool == null || carpool.getDriverId() == null){
			return Result.fail("车主id不能为空！");
		}
		Integer driverId = carpool.getDriverId();
		DriverInfo driverInfo = driverInfoDao.queryDriverInfoByPrimaryKey(driverId);
		if(driverInfo == null){
			return Result.fail("车主id不正确！");
		}
		return Result.success();
	}
}

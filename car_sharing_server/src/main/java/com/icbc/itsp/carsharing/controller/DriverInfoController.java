package com.icbc.itsp.carsharing.controller;

import com.alibaba.fastjson.JSONObject;
import com.icbc.itsp.carsharing.entity.Address;
import com.icbc.itsp.carsharing.entity.DriverInfo;
import com.icbc.itsp.carsharing.entity.User;
import com.icbc.itsp.carsharing.response.Result;
import com.icbc.itsp.carsharing.service.AddressService;
import com.icbc.itsp.carsharing.service.DriverInfoService;
import com.icbc.itsp.carsharing.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/driverInfo")
@CrossOrigin
@Slf4j
public class DriverInfoController {
    @Resource
    private DriverInfoService driverInfoService;
    @Resource
    private UserService userService;
    @Resource
    private AddressService addressService;

    /**
     * 查询车主信息
     * @param driverInfo
     * @return
     */
    @RequestMapping(value = "/query",method = RequestMethod.GET)
    public Result queryDriverInfo(DriverInfo driverInfo){
        List<DriverInfo> driverResults = driverInfoService.queryDriverInfo(driverInfo);
        String jsonString = JSONObject.toJSONString(driverResults);
        return Result.success(driverResults);
    }

    /**
     * 根据userId查询一条车主信息
     * @param userId
     * @return
     */
    @RequestMapping(value="/findOneDriverInfoByOpenid",method = RequestMethod.GET)
    public Result findOneDriverInfoByOpenid(@RequestParam("openid") String userId){
        DriverInfo driverInfo = driverInfoService.queryDriverInfoByUniqueKey(userId);
        return Result.success(driverInfo);
    }

    /**
     * 车主个人信息新增
     * @param driverInfo
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addDriverInfo(@RequestBody DriverInfo driverInfo) {
        if (driverInfo == null || StringUtils.isEmpty(driverInfo.getOpenid())) {
            return Result.fail("openid不能为空！");
        }

        User user = userService.queryUserByUniqueKey(driverInfo.getOpenid());
        if (user == null) {
            return Result.fail("用户信息不存在，不能新增车主信息！");
        }
        DriverInfo queryResult = driverInfoService.queryDriverInfoByUniqueKey(driverInfo.getOpenid());
        //已存在车主信息
        if (queryResult != null) {
            return Result.fail("车主信息已存在！");
        }

        //更新用户
        Result updateUserResult = updateUser(driverInfo);
        if (!updateUserResult.isSuccess()) {
            return updateUserResult;
        }
        //更新或新增地址
        Result updateOrAddAddressResult = updateOrAddAddress(driverInfo);
      /*  if (!updateOrAddAddressResult.isSuccess()) {
            return updateOrAddAddressResult;
        }*/

        boolean result = driverInfoService.addDriverInfo(driverInfo);

        if (result) {
            return Result.success();
        } else {
            return Result.fail("车主信息新增失败！", driverInfo);
        }
    }

    /**
     * 车主更新个人信息
     * @param driverInfo
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Result updateDriverInfo(@RequestBody DriverInfo driverInfo) {
        if (driverInfo == null || StringUtils.isEmpty(driverInfo.getOpenid())) {
            return Result.fail("openid不能为空！");
        }
        DriverInfo queryResult = driverInfoService.queryDriverInfoByUniqueKey(driverInfo.getOpenid());
        if (queryResult == null) {
            return Result.fail("车主信息不存在！");
        }
        //更新用户
        Result updateUserResult = updateUser(driverInfo);
        if (!updateUserResult.isSuccess()) {
            return updateUserResult;
        }
        //更新或新增地址
        Result updateOrAddAddressResult = updateOrAddAddress(driverInfo);
       /* if (!updateOrAddAddressResult.isSuccess()) {
            return updateOrAddAddressResult;
        }*/
        boolean result = driverInfoService.updateDriverInfo(driverInfo);

        if (result) {
            return Result.success(driverInfo);
        } else {
            return Result.fail("车主信息更新失败！", driverInfo);
        }
    }

    /**
     * 更新用户
     * @param driverInfo
     * @return
     */
    private Result updateUser(DriverInfo driverInfo){
        Result result = Result.success();
        //更新用户表信息
        if(driverInfo.getUser() != null){
            User userUpdate = driverInfo.getUser();
            userUpdate.setOpenid(driverInfo.getOpenid());
            result = userService.updateUser(userUpdate);
        }
        return result;
    }

    /**
     * 更新获新增地址
     * @param driverInfo
     * @return
     */
    private Result updateOrAddAddress(DriverInfo driverInfo){
        Result result = Result.success();
        //新增/更新地址
        if(driverInfo.getToWorkStartLocationAddress() != null){
            Address address = driverInfo.getToWorkStartLocationAddress();
            if(address.getId() != null){
                result = addressService.updateAddress(address);
                driverInfo.setToWorkStartLocation(address.getId());
            }else{
                result = addressService.addAddress(address);
                driverInfo.setToWorkStartLocation(address.getId());
            }
        }
        //新增/更新地址
        if(driverInfo.getAfterWorkStartLocationAddress() != null){
            Address address = driverInfo.getAfterWorkStartLocationAddress();
            if(address.getId() != null){
                result = addressService.updateAddress(address);
                driverInfo.setAfterWorkStartLocation(address.getId());
            }else{
                result = addressService.addAddress(address);
                driverInfo.setAfterWorkStartLocation(address.getId());
            }
        }
        return result;
    }
}

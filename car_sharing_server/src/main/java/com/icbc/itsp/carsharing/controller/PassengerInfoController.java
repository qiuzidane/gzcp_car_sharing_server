package com.icbc.itsp.carsharing.controller;

import com.icbc.itsp.carsharing.entity.Address;
import com.icbc.itsp.carsharing.entity.PassengerInfo;
import com.icbc.itsp.carsharing.entity.User;
import com.icbc.itsp.carsharing.response.Result;
import com.icbc.itsp.carsharing.service.AddressService;
import com.icbc.itsp.carsharing.service.PassengerInfoService;
import com.icbc.itsp.carsharing.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/passengerInfo")
@CrossOrigin
@Slf4j
public class PassengerInfoController {
    @Resource
    private PassengerInfoService passengerInfoService;
    @Resource
    private UserService userService;
    @Resource
    private AddressService addressService;

    /**
     * 查询乘客个人信息
     * @param passengerInfo
     * @return
     */
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public Result queryPassengerInfo(PassengerInfo passengerInfo) {
        //车主id不为空时，只查询车主本人的信息，否则查询全部乘客信息
        List<PassengerInfo> passengerResults = passengerInfoService.queryPassengerInfo(passengerInfo);
        return Result.success(passengerResults);
    }

    /**
     * 根据userId查询一条乘客信息
     * @param userId
     * @return
     */
    @RequestMapping(value="/findOnePassengerInfoByOpenid",method = RequestMethod.GET)
    public Result findOneDriverInfoByOpenid(@RequestParam("openid") String userId){
        PassengerInfo passengerInfo = passengerInfoService.queryPassengerInfoByUniqueKey(userId);
        return Result.success(passengerInfo);
    }

    /**
     * 新增乘车个人信息
     * @param passengerInfo
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result addPassengerInfo(@RequestBody PassengerInfo passengerInfo) {
        if (passengerInfo == null || StringUtils.isEmpty(passengerInfo.getOpenid())) {
            return Result.fail("openid不可为空！");
        }

        User user = userService.queryUserByUniqueKey(passengerInfo.getOpenid());
        if (user == null) {
            return Result.fail("用户信息不存在，不能新增乘客信息！");
        }

        PassengerInfo queryResult = passengerInfoService.queryPassengerInfoByUniqueKey(passengerInfo.getOpenid());
        if (queryResult != null) {
            return Result.fail("乘客信息已存在！");
        }
        //更新用户
        Result updateUserResult = updateUser(passengerInfo);
        if(!updateUserResult.isSuccess()){
            return updateUserResult;
        }
        //更新获新增地址
        Result updateOrAddAddressResult = updateOrAddAddress(passengerInfo);
        /*if(!updateOrAddAddressResult.isSuccess()){
            return updateOrAddAddressResult;
        }*/
        boolean result = passengerInfoService.addPassengerInfo(passengerInfo);

        if (result) {
            return Result.success();
        } else {
            return Result.fail("乘客信息添加失败！", passengerInfo);
        }
    }

    /**
     * 更新乘客个人信息
     * @param passengerInfo
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Result savePassengerInfo(@RequestBody PassengerInfo passengerInfo) {
        if (passengerInfo == null || StringUtils.isEmpty(passengerInfo.getOpenid())) {
            return Result.fail("参数不符！");
        }
        PassengerInfo queryResult = passengerInfoService.queryPassengerInfoByUniqueKey(passengerInfo.getOpenid());
        if (queryResult == null) {
            return Result.fail("乘客信息不存在！");
        }

        //更新用户
        Result updateUserResult = updateUser(passengerInfo);
        if(!updateUserResult.isSuccess()){
            return updateUserResult;
        }
        //更新获新增地址
        Result updateOrAddAddressResult = updateOrAddAddress(passengerInfo);
        /*if(!updateOrAddAddressResult.isSuccess()){
            return updateOrAddAddressResult;
        }*/

        boolean result = passengerInfoService.updatePassengerInfo(passengerInfo);

        if (result) {
            return Result.success();
        } else {
            return Result.fail(404, "乘客信息保存失败！", passengerInfo);
        }
    }

    /**
     * 更新用户
     * @param passengerInfo
     * @return
     */
    private Result updateUser(PassengerInfo passengerInfo){
        Result result = Result.success();
        //更新用户表信息
        if(passengerInfo.getUser() != null){
            User userUpdate = passengerInfo.getUser();
            userUpdate.setOpenid(passengerInfo.getOpenid());
            result = userService.updateUser(userUpdate);
        }
        return result;
    }

    /**
     * 更新获新增地址
     * @param passengerInfo
     * @return
     */
    private Result updateOrAddAddress(PassengerInfo passengerInfo){
        Result result = Result.success();
        //新增/更新地址
        if(passengerInfo.getStartLocationAddress() != null){
            Address address = passengerInfo.getStartLocationAddress();
            if(address.getId() != null){
                result = addressService.updateAddress(address);
                passengerInfo.setStartLocation(address.getId());
            }else{
                result = addressService.addAddress(address);
                passengerInfo.setStartLocation(address.getId());
            }
        }
        return result;
    }
}

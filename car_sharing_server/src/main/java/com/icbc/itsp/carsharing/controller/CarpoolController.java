package com.icbc.itsp.carsharing.controller;

import com.icbc.itsp.carsharing.entity.Address;
import com.icbc.itsp.carsharing.entity.Carpool;
import com.icbc.itsp.carsharing.entity.DriverInfo;
import com.icbc.itsp.carsharing.response.Result;
import com.icbc.itsp.carsharing.service.AddressService;
import com.icbc.itsp.carsharing.service.CarpoolService;
import com.icbc.itsp.carsharing.service.DriverInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/carpool")
@CrossOrigin
@Slf4j
public class CarpoolController {
    @Resource
    private CarpoolService carpoolService;
    @Resource
    private AddressService addressService;
    @Resource
    private DriverInfoService driverInfoService;

    /**
     * 拼车总单查询接口
     * @param carpool
     * @return
     */
    @RequestMapping(value = "/query",method = RequestMethod.GET)
    public Result queryCarpool(Carpool carpool){
        try{
            List<Carpool> list = carpoolService.queryCarpool(carpool);
            return Result.success(list);
        }catch (Exception e){
            e.printStackTrace();
            log.error("查询拼车总表记录发生异常：",e);
            return Result.fail("查询拼车总表记录发生异常！");
        }
    }

    /**
     * 车主发布拼车
     * @return
     */
    @RequestMapping(value = "/release",method = RequestMethod.POST)
    public Result releaseCarpool(@RequestBody Carpool carpool){
        try{
            if(carpool == null){
                return Result.fail("拼车总表不能为空！");
            }
            if(carpool.getDriverId() == null){
                return Result.fail("请维护车主信息!");
            }
            DriverInfo driverInfo = driverInfoService.queryDriverInfoByPrimaryKey(carpool.getDriverId());
            if(driverInfo == null){
                return Result.fail("请维护车主信息!");
            }

            if(carpool.getStartLocationAddress() != null && carpool.getStartLocation() == null){
                Address startLocationAddress = carpool.getStartLocationAddress();
                addressService.addAddress(startLocationAddress);
                carpool.setStartLocation(startLocationAddress.getId());
            }
            if(carpool.getEndLocationAddress() != null && carpool.getEndLocation() == null){
                Address endLocationAddress = carpool.getEndLocationAddress();
                addressService.addAddress(endLocationAddress);
                carpool.setEndLocation(endLocationAddress.getId());
            }
            Result result = carpoolService.releaseCarpool(carpool);
            return result;
        }catch (Exception e){
            e.printStackTrace();
            log.error("发布拼车发生异常：",e);
            return Result.fail("发布拼车发生异常！");
        }
    }

    /**
     * 修改拼车总表记录
     * @param carpool
     * @return
     */
    @RequestMapping(value = "/update" , method = RequestMethod.POST)
    public Result update (@RequestBody Carpool carpool){
        try {
            if(carpool == null || carpool.getId() == null){
                return Result.fail("主键id不能为空！");
            }
            //如果地址id不为空，更新地址；如果哦地址id新增地址
            if(carpool.getStartLocationAddress() != null){
                Integer startAddressId = carpool.getStartLocationAddress().getId();
                if( startAddressId!= null){
                    Address startLocationAddress = carpool.getStartLocationAddress();
                    addressService.updateAddress(startLocationAddress);
                }else{
                    Address startLocationAddress = carpool.getStartLocationAddress();
                    addressService.addAddress(startLocationAddress);
                    carpool.setStartLocation(startLocationAddress.getId());
                }
            }
            if(carpool.getEndLocationAddress() != null){
                Integer startAddressId = carpool.getEndLocationAddress().getId();
                if( startAddressId!= null){
                    Address endLocationAddress = carpool.getEndLocationAddress();
                    addressService.updateAddress(endLocationAddress);
                }else{
                    Address endLocationAddress = carpool.getEndLocationAddress();
                    addressService.addAddress(endLocationAddress);
                    carpool.setEndLocation(endLocationAddress.getId());
                }
            }

            Result result = carpoolService.update(carpool);
            return result;
        }catch (Exception e){
            e.printStackTrace();
            log.error("修改拼车总表记录发生异常：",e);
            return Result.fail("修改拼车总表记录发生异常！");
        }
    }

    /**
     * 1、取消未结束的拼车
     * 2、完成拼车
     * @param carpool
     * @return
     */
    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    public Result deleteCarpool(Carpool carpool) {
        if(carpool == null || StringUtils.isEmpty(carpool.getId())){
            return Result.fail("拼车总单的id不能为空!");
        }
        boolean flag = carpoolService.deleteCarpool(carpool);
        if(flag){
            return Result.success();
        }
        return Result.fail("删除失败，请重试!");
    }
}

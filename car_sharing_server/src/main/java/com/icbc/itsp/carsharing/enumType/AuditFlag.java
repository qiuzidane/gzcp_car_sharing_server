package com.icbc.itsp.carsharing.enumType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum AuditFlag {
    //0-小程序审核前 ； 1-小程序审核后
    BEFORE_AUDIT("0"),
    AFTER_AUDIT("1");
    private String flag;
}

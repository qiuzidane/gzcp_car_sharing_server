package com.icbc.itsp.carsharing.service;

import com.icbc.itsp.carsharing.entity.Carpool;
import com.icbc.itsp.carsharing.response.Result;
import java.util.List;
import java.util.Map;

public interface CarpoolService {
    List<Carpool> queryCarpool(Carpool carpool);

    Result releaseCarpool(Carpool carpool);

    Result update (Carpool carpool);

    boolean deleteCarpool(Carpool carpool);

    /**
     *
     * @param idList
     * @return
     */
    List<Carpool> findCarpoolByIds(Map<String, Object> idList);
}

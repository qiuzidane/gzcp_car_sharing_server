package com.icbc.itsp.carsharing.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * 拼车总表
 */
@Data
public class Carpool {
    private Integer id;
    private Integer driverId;//车主个人信息表id
    private String carNo;   // 车牌号
    private Integer startLocation;     // 发车地点 -地址信息保存到地址表，保存地址id
    private Integer endLocation;     // 终点 -地址信息保存到地址表，保存地址id
    private String line; //线路描述
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime; //发车时间
    private Integer totalSeat; //总共可拼乘客数量
    private String status; //状态
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime; //发布时间

    @Transient
    private Address startLocationAddress; //发车地址对象
    @Transient
    private Address endLocationAddress; //终点地址对象
    @Transient
    private DriverInfo driverInfo; //车主个人信息对象
    @Transient
    private User user;  //用户对象
    @Transient
    private List<CarpoolDtl> carpoolDtlList; //拼车明细

    @Transient
    private Integer carpoolSuccessSeat; //已拼车人数
    @Transient
    private String remark; //取消拼车备注
}

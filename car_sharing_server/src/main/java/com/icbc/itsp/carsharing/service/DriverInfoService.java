package com.icbc.itsp.carsharing.service;

import com.icbc.itsp.carsharing.entity.DriverInfo;
import java.util.List;

public interface DriverInfoService {
    /**
     * 根据openid查询单条
     * @param openid
     * @return
     */
    DriverInfo queryDriverInfoByUniqueKey(String openid);

    /**
     * 根据id查询单条
     * @param id
     * @return
     */
    DriverInfo queryDriverInfoByPrimaryKey(Integer id);

    /**
     * 多条查询
     * @return
     */
    List<DriverInfo> queryDriverInfo(DriverInfo driverInfo);

    /**
     * 新增
     * @param driverInfo
     * @return
     */
    boolean addDriverInfo(DriverInfo driverInfo);

    /**
     * 更新
     * @param driverInfo
     * @return
     */
    boolean updateDriverInfo(DriverInfo driverInfo);
}

package com.icbc.itsp.carsharing.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@Slf4j
public class WebSiteController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<meta charset=\"utf-8\">\n" +
                "<title>我的博客</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <h1>欢迎进入我的博客</h1>\n" +
                "    <p>您是第一位读者</p >\n" +
                "</body>\n" +
                "</html>";
    }
}

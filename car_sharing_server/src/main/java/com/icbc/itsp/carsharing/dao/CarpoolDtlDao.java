package com.icbc.itsp.carsharing.dao;

import com.icbc.itsp.carsharing.entity.CarpoolDtl;
import com.icbc.itsp.carsharing.entity.QueryParamVo;

import java.util.List;

public interface CarpoolDtlDao {
    CarpoolDtl queryByPrimary(Integer id);
    
    /**
     * 查询拼车明细
     */
    List<CarpoolDtl> queryCarpoolDtl(CarpoolDtl carpoolDtl);

    /**
     * 新增拼车明细
     */
    Integer addCarpoolDtl(CarpoolDtl carpoolDtl);

    /**
     * 删除拼车明细
     */
    Integer deleteCarpoolDtl(CarpoolDtl carpoolDtl);

    List<String> findCarpoolIds (CarpoolDtl carpoolDtl);

    List<CarpoolDtl> queryCarpoolDtlByDriverOpenid(QueryParamVo queryParamVo);

    List<CarpoolDtl> queryCarpoolDtlByPassengerOpenid(QueryParamVo queryParamVo);

    /**
     * 更新
     */
    Integer updateCarpoolDtl(CarpoolDtl carpoolDtl);

    /**
     * 座位不足更新剩余拼车明细 为失败
     */
    Integer updateFailedCarpool(CarpoolDtl carpoolDtl);

    /**
     * 根据乘客openid查询待出行行程数量
     * @param queryParamVo
     * @return
     */
    Integer countStartOff(QueryParamVo<CarpoolDtl> queryParamVo);

    /**
     * 根据车主openid查询待审批拼车明细数量
     * @param queryParamVo
     * @return
     */
    Integer countPending(QueryParamVo<CarpoolDtl> queryParamVo);

}

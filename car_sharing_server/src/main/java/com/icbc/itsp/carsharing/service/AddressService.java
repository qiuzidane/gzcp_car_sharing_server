package com.icbc.itsp.carsharing.service;

import com.icbc.itsp.carsharing.entity.Address;
import com.icbc.itsp.carsharing.response.Result;
import java.util.List;

public interface AddressService {
    /**
     * 查询地址
     * @param address
     * @return
     */
    List<Address> queryAddress(Address address);
    Result addAddress(Address address);
    Result updateAddress(Address address);
    Integer deleteAddress(Address address);
}

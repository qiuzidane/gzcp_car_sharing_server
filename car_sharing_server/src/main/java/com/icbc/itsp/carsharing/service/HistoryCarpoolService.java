package com.icbc.itsp.carsharing.service;

import com.icbc.itsp.carsharing.entity.HistoryCarpool;
import java.util.List;
import java.util.Map;

public interface HistoryCarpoolService {
    /**
     * 查询已结束的拼车总表记录
     * @param historyCarpool
     * @return
     */
    List<HistoryCarpool> queryHistoryCarpool(HistoryCarpool historyCarpool);

    /**
     *
     * @param idList
     * @return
     */
    List<HistoryCarpool> findHistoryCarpoolByIds(Map<String, Object> idList);
}

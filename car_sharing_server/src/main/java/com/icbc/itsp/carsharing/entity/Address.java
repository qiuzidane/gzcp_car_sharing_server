package com.icbc.itsp.carsharing.entity;

import lombok.Data;

/**
 * 地址信息
 */
@Data
public class Address {
    private Integer id;
    private String city;  // 城市
    private String addressName;//地址名
    private String longitude; //经度
    private String latitude; //纬度
    private String addressDetail; //详细地址
}

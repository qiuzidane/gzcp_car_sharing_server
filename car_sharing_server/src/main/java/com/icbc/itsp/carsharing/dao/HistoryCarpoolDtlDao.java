package com.icbc.itsp.carsharing.dao;

import com.icbc.itsp.carsharing.entity.HistoryCarpoolDtl;

import java.util.List;

public interface HistoryCarpoolDtlDao {
	List<HistoryCarpoolDtl> queryHistoryCarpoolDtl(HistoryCarpoolDtl historyCarpoolDtl);
	void insert(HistoryCarpoolDtl historyCarpoolDtl);
	List<String> findCarpoolIds (HistoryCarpoolDtl historyCarpoolDtl);
}

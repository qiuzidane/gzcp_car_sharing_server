package com.icbc.itsp.carsharing.service.serviceImpl;

import com.icbc.itsp.carsharing.dao.UserDao;
import com.icbc.itsp.carsharing.entity.QueryParamVo;
import com.icbc.itsp.carsharing.entity.User;
import com.icbc.itsp.carsharing.enumType.AuditFlag;
import com.icbc.itsp.carsharing.enumType.EmployeeFlag;
import com.icbc.itsp.carsharing.enumType.EmployeeLevel;
import com.icbc.itsp.carsharing.response.Result;
import com.icbc.itsp.carsharing.service.UserService;
import com.icbc.itsp.carsharing.tools.HttpsClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
@PropertySource({"classpath:application.yml"})
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;
    @Value("${user.openid}")
    private String openids;
    @Value("${user.auditFlag}")
    private String auditFlag;

    @Override
    public List<User> queryUser(QueryParamVo<User> queryParamVo){
        List<User> list = userDao.queryUser(queryParamVo);
        return list;
    }

    @Override
    public User queryUserByUniqueKey(String openid){
        User user = userDao.queryUserByUniqueKey(openid);
        return user;
    }

    @Override
    public User queryUserByPrimaryKey(Integer id){
        User user = userDao.queryUserByPrimaryKey(id);
        return user;
    }

    @Override
    public Result addUser(User user){
        //检查唯一索引
        User queryUserByUniqueKey = userDao.queryUserByUniqueKey(user.getOpenid());
        if(queryUserByUniqueKey != null){
            Result result = Result.fail("用户已存在，不能新增用户！");
            result.setData(queryUserByUniqueKey);
            return result;
        }

        if(user == null || StringUtils.isEmpty(user.getOpenid())){
            Result result = Result.fail("openid不能为空！");
            result.setData(queryUserByUniqueKey);
            return result;
        }
        if(!AuditFlag.BEFORE_AUDIT.getFlag().equals(auditFlag) && !AuditFlag.AFTER_AUDIT.getFlag().equals(auditFlag)){
            Result result = Result.fail("配置参数不正确，小程序审核标志志只能配置为：0/1");
            result.setData(queryUserByUniqueKey);
            return result;
        }

        //新增的时候，openid配置在yml的管理员列表里，直接改员工等级为管理员
        List<String> openidList = Arrays.asList(openids.split(","));
        if(openidList.contains(user.getOpenid())){
            user.setEmployeeLevel(EmployeeLevel.ADMIN.getLevle());
            user.setEmployeeFlag(EmployeeFlag.IS_EMPLOYEE.getFlag());
        }
        //小程序审核前所有新用户默认设置员工标志=0-员工；员工级别=0-普通员工。
        user.setAuditFlag(auditFlag);
        if(AuditFlag.BEFORE_AUDIT.getFlag().equals(auditFlag)){
            user.setEmployeeFlag(EmployeeFlag.IS_EMPLOYEE.getFlag());
            if(!EmployeeLevel.ADMIN.getLevle().equals(user.getEmployeeLevel())){
                user.setEmployeeLevel(EmployeeLevel.COMMON.getLevle());
            }
        }else {
            //检查员工标志输入值是否正确
            Result resultFlage = checkEmployeeFlag(user);
            if(!resultFlage.isSuccess()){
                resultFlage.setData(queryUserByUniqueKey);
                return resultFlage;
            }
            //检查员工级别输入值是否正确
            Result resultLevel = checkEmployeeLevel(user);
            if(!resultLevel.isSuccess()){
                resultFlage.setData(queryUserByUniqueKey);
                return resultLevel;
            }
        }
        userDao.insertUser(user);
        User newUser = userDao.queryUserByUniqueKey(user.getOpenid());
        return Result.success(newUser);
    }

    @Override
    public Integer deleteUser(User user){
        Integer i = userDao.deleteByPrimaryKeySelective(user);
        return i;
    }

    @Override
    public Result updateUser(User user){
        if(user == null || StringUtils.isEmpty(user.getOpenid())){
            Result result = Result.fail("openid和id不能为空！");
            result.setData(user);
            return result;
        }
        //检查openid是否存在数据库
        User queryUserByUniqueKey = userDao.queryUserByUniqueKey(user.getOpenid());
        if(queryUserByUniqueKey == null){
            Result result = Result.fail("用户不存在，更新失败！");
            result.setData(queryUserByUniqueKey);
            return result;
        }
        //检查员工标志输入值是否正确，如果未传值，设置默认值
        if(!StringUtils.isEmpty(user.getEmployeeFlag())){
            Result result = checkEmployeeFlag(user);
            if(!result.isSuccess()){
                result.setData(queryUserByUniqueKey);
                return result;
            }
        }
        //检查员工级别输入值是否正确，如果未传值，设置默认值
        if(!StringUtils.isEmpty(user.getEmployeeLevel())){
            Result result = checkEmployeeLevel(user);
            if(!result.isSuccess()){
                result.setData(queryUserByUniqueKey);
                return result;
            }
        }

        //       武杰说先判断前端传过来的user，去数据库查询原来的user.employeeLevel是不是“9-管理员”：
        //       1. 如果原来的user.employeeLevel是“9-管理员”： 且user.employeeFlag是“0-员工”的话，就可以改user.employeeLevel。
        //       2. 如果原来的user.employeeFlag不是“0-员工”，不管前端送的user.employeeLevel是什么值，都不要更新user.employeeLevel
        User checkEmployeeLevel =  userDao.queryUserByUniqueKey(user.getOpenid());
        if(EmployeeLevel.ADMIN.getLevle().equals(checkEmployeeLevel.getEmployeeLevel())
                && EmployeeFlag.IS_EMPLOYEE.getFlag().equals(checkEmployeeLevel.getEmployeeFlag())){
            //可以更新员工级别
        }else if (!EmployeeFlag.IS_EMPLOYEE.getFlag().equals(user.getEmployeeFlag())){
            //员工级别只能为默认值0
            user.setEmployeeLevel(EmployeeLevel.COMMON.getLevle());
        }

        //以下是作者的想法：根据openid查询数据库，如果该用户修改之前是管理员，则不修改员工级别employeeLevel
        /*if(EmployeeLevel.ADMIN.getLevle().equals(checkEmployeeLevel.getEmployeeLevel())){
            user.setEmployeeLevel(null);
        }*/
        Integer i = userDao.updateByUniqueKeySelective(user);
        User newUser = userDao.queryUserByUniqueKey(user.getOpenid());
        return Result.success(newUser);
    }

    /**
     * 检查员工标志输入值是否正确
     * @param user
     * @return
     */
    private Result checkEmployeeFlag(User user){
        if(!StringUtils.isEmpty(user.getEmployeeFlag())
                && !EmployeeFlag.IS_EMPLOYEE.getFlag().equals(user.getEmployeeFlag())
                && !EmployeeFlag.NOT_EMPLOYEE.getFlag().equals(user.getEmployeeFlag())
                && !EmployeeFlag.PENDING.getFlag().equals(user.getEmployeeFlag())){
            return Result.fail("员工标志只能传参数：0/1/2/空值！");
        }else if(StringUtils.isEmpty(user.getEmployeeFlag())){
            user.setEmployeeFlag(EmployeeFlag.PENDING.getFlag());
        }

        return Result.success(user);
    }

    /**
     * 检查员工员工级别输入值是否正确
     * @param user
     * @return
     */
    private Result checkEmployeeLevel(User user){
        if(!StringUtils.isEmpty(user.getEmployeeLevel())
                && !EmployeeLevel.COMMON.getLevle().equals(user.getEmployeeLevel())
                && !EmployeeLevel.LEADER.getLevle().equals(user.getEmployeeLevel())
                && !EmployeeLevel.ADMIN.getLevle().equals(user.getEmployeeLevel())){
            return Result.fail("员工级别只能传参数：0/7/9/空值！");
        }else if (StringUtils.isEmpty(user.getEmployeeLevel()) ){
            user.setEmployeeLevel(EmployeeLevel.COMMON.getLevle());
        }
        return Result.success(user);
    }

    @Override
    public Result getOpenid(String jsCode){
        try {
            if(StringUtils.isEmpty(jsCode)){
                return Result.fail("jsCode为空，请检查登录请求的参数！");
            }
            String appid = "wxd59aa19da0173ef4";
            StringBuffer requestUrl = new StringBuffer("https://api.weixin.qq.com/sns/jscode2session?appid=wxd59aa19da0173ef4&secret=605dc059f3b40a786d7488197abb49da&grant_type=authorization_code");
            requestUrl.append("&js_code=").append(jsCode);
            HttpHeaders header = new HttpHeaders();
            header.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            header.set("Authorization", "appid " + appid);
            //添加请求头
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(header);
            //发送post请求
            ResponseEntity<String> response = HttpsClient.restTemplate().postForEntity(requestUrl.toString(), request, String.class);
            log.info("response:" + response.toString());
            return Result.success(response.getBody());
        }catch (Exception e){
            e.printStackTrace();
            log.error("获取openid发生异常：",e);
            return Result.fail("获取openid发生异常");
        }
    }

    @Override
    public Integer queryUserCount(QueryParamVo<User> queryParamVo){
        Integer i = userDao.queryUserCount(queryParamVo);
        return i;
    }

}

package com.icbc.itsp.carsharing.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * 接口返回分页通用类
 *
 * @param <T>
 * @author ghy
 * @date 2020-02-09
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Page<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 当前页码
     */
    private int pageNo = 1;

    /**
     * 每页数量
     */
    private int pageSize = 20;

    /**
     * 总数量
     */
    private int totalCount = 0;

    /**
     * 结果集
     */
    private List<T> results = null;

    /**
     * 总页数
     */
    private int totalPages = 0;

    /**
     * 条件参数集
     */
    private Map<String, Object> params = null;

    /**
     * 多字段排序集
     */
    private List<Sort> sorts = null;

    public Page(int pageNo, int pageSize, int totalCount, List<T> results, int totalPages, Map<String, Object> params, List<Sort> sorts) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.totalCount = totalCount;
        this.results = results;
        this.totalPages = totalPages;
        this.params = params;
        this.sorts = sorts;
    }


    private Page() {
    }

    private Page(Map<String, Object> params) {
        if (params == null) {
            params = new HashMap<String, Object>();
        }
        results = new ArrayList<>();
        sorts = new ArrayList<>();
        this.params = params;
    }

    private Page(int pageNo, Map<String, Object> params) {
        this(params);
        this.pageNo = pageNo;
    }

    private Page(int pageNo, int pageSize, Map<String, Object> params) {
        this(pageNo, params);
        this.pageSize = pageSize;
    }

    public int getPageNo() {
        return pageNo;
    }

    public Page<T> setPageNo(int pageNo) {
        this.pageNo = pageNo;
        return this;
    }

    public int getPageSize() {
        return pageSize;
    }

    public Page<T> setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public Page<T> setTotalCount(int totalCount) {
        this.totalCount = totalCount;
        return this;
    }

    public List<T> getResults() {
        return results;
    }

    public Page<T> setResults(List<T> results) {
        this.results = results;
        return this;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public Page<T> setParams(Map<String, Object> params) {
        this.params = params;
        return this;
    }

    public int getTotalPages() {
        int pageSize = getPageSize();
        int totalCount = getTotalCount();
        totalPages = totalCount / pageSize;
        if ((totalCount % pageSize) > 0)
            ++totalPages;
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<Sort> getSorts() {
        return sorts;
    }

    public Page<T> setSorts(List<Sort> sorts) {
        this.sorts = sorts;
        return this;
    }

    /**
     * 添加排序字段
     *
     * @param field 字段名称
     * @param sort  排序类型
     * @return
     */
    public Page<T> addSort(String field, SortEnum sort) {
        this.sorts.add(new Sort(field, sort));
        return this;
    }

    /**
     * 添加查询参数
     *
     * @param key 参数键
     * @param obj 参数值
     * @return
     */
    public Page<T> put(String key, Object obj) {
        this.params.put(key, obj);
        return this;
    }

    public static <T> Page<T> newInstance() {
        return new Page<T>();
    }

    public static <T> Page<T> newInstance(Map<String, Object> params) {
        return new Page<T>(params);
    }

    public static <T> Page<T> newInstance(int pageNo, Map<String, Object> params) {
        return new Page<T>(pageNo, params);
    }

    public static <T> Page<T> newInstance(int pageNo, int pageSize, Map<String, Object> params) {
        return new Page<T>(pageNo, pageSize, params);
    }

    public class Sort implements Serializable {
        /**
         *
         */
        private static final long serialVersionUID = -9048497004035410678L;

        /**
         * 排序字段
         */
        private String field;

        /**
         * 排序类型
         */
        private SortEnum sort;

        private Sort(String field, SortEnum sort) {
            this.field = field;
            this.sort = sort;
        }

        public String getField() {
            return field;
        }

        public void setField(String field) {
            this.field = field;
        }

        public SortEnum getSort() {
            return sort;
        }

        public void setSort(SortEnum sort) {
            this.sort = sort;
        }

    }

    public enum SortEnum {
        ASC("asc"), DESC("desc");
        private String sort = "desc";

        private SortEnum(String sort) {
            this.sort = sort;
        }

        public String getSort() {
            return sort;
        }
    }
}

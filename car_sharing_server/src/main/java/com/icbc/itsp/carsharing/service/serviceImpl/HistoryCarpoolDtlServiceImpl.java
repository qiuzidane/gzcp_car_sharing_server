package com.icbc.itsp.carsharing.service.serviceImpl;

import com.icbc.itsp.carsharing.dao.*;
import com.icbc.itsp.carsharing.entity.*;
import com.icbc.itsp.carsharing.service.HistoryCarpoolDtlService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HistoryCarpoolDtlServiceImpl implements HistoryCarpoolDtlService {
    @Resource
    private HistoryCarpoolDtlDao historyCarpoolDtlDao;
    @Resource
    private HistoryCarpoolDao historyCarpoolDao;
    @Resource
    private CarpoolDao carpoolDao;
    @Resource
    private AddressDao addressDao;
    @Resource
    private CommonDao commonDao;
    @Resource
    private DriverInfoDao driverInfoDao;
    @Resource
    private UserDao userDao;

    @Override
    public List<HistoryCarpoolDtl> queryHistoryCarpoolDtl(HistoryCarpoolDtl historyCarpoolDtl) {
        List<HistoryCarpoolDtl> list = historyCarpoolDtlDao.queryHistoryCarpoolDtl(historyCarpoolDtl);
        for(HistoryCarpoolDtl historyCarpoolDtlTemp : list){
            HistoryCarpool historyCarpool = historyCarpoolDao.queryByOrgCarpoolId(historyCarpoolDtlTemp.getCarpoolId());
            if(historyCarpool != null && historyCarpool.getStartLocation() != null){
                Address carStartLocationAddress = addressDao.queryAddressByPrimaryKey(historyCarpool.getStartLocation());
                historyCarpoolDtlTemp.setCarStartLocationAddress(carStartLocationAddress);
            }
            if(historyCarpoolDtlTemp.getStartLocation() != null){
                Address startLocationAddress = addressDao.queryAddressByPrimaryKey(historyCarpoolDtlTemp.getStartLocation());
                historyCarpoolDtlTemp.setStartLocationAddress(startLocationAddress);
            }
            if(historyCarpoolDtlTemp.getEndLocation() != null){
                Address endLocationAddress = addressDao.queryAddressByPrimaryKey(historyCarpoolDtlTemp.getEndLocation());
                historyCarpoolDtlTemp.setEndLocationAddress(endLocationAddress);
            }
            if(historyCarpoolDtlTemp.getPassengerId() != null){
                User user = commonDao.queryUserByPassengerId(historyCarpoolDtlTemp.getPassengerId());
                historyCarpoolDtlTemp.setUser(user);
            }
        }
        return list;
    }

    @Override
    public  List<HistoryCarpool> queryCarpoolByPassengerId(HistoryCarpoolDtl historyCarpoolDtl){
        List<HistoryCarpool> list = new ArrayList<>();
        List<String> carpoolIdList = historyCarpoolDtlDao.findCarpoolIds(historyCarpoolDtl);
        if(carpoolIdList.size() <= 0){
            return list;
        }
        Map<String,Object> map = new HashMap();
        map.put("idList", carpoolIdList);
        list = historyCarpoolDao.findHistoryCarpoolByIds(map);
        //如果历史总单查不到数据，再查拼车总单，乘客自己取消的拼车明细，总单不一定结束或者取消，还进入拼车历史
        if(list.size() <= 0){
            List<Carpool> carpoolList = carpoolDao.findCarpoolByIds(map);
            for(Carpool carpoolTemp : carpoolList){
                HistoryCarpool historyCarpool = new HistoryCarpool();
                BeanUtils.copyProperties(carpoolTemp,historyCarpool);
                historyCarpool.setOrgCarpoolId(carpoolTemp.getId());
                list.add(historyCarpool);
            }
        }
        for (HistoryCarpool historyCarpoolTemp : list){
            if(historyCarpoolTemp.getStartLocation() != null){
                Address startAddress = addressDao.queryAddressByPrimaryKey(historyCarpoolTemp.getStartLocation());
                historyCarpoolTemp.setStartLocationAddress(startAddress);
            }
            if(historyCarpoolTemp.getEndLocation() != null){
                Address endAddress = addressDao.queryAddressByPrimaryKey(historyCarpoolTemp.getEndLocation());
                historyCarpoolTemp.setEndLocationAddress(endAddress);
            }
            if(historyCarpoolTemp.getDriverId() != null){
                DriverInfo driverInfo = driverInfoDao.queryDriverInfoByPrimaryKey(historyCarpoolTemp.getDriverId());
                if(driverInfo != null){
                    User user = userDao.queryUserByUniqueKey(driverInfo.getOpenid());
                    historyCarpoolTemp.setUser(user);
                }
            }
        }
        return list;
    }
}

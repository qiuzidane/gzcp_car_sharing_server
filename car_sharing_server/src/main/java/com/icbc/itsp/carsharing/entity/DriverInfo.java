package com.icbc.itsp.carsharing.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Transient;
import java.util.Date;

/**
 * 司机信息
 */
@Data
@Setter
@Getter
public class DriverInfo {
    private Integer id;
    private String openid;  //微信用户id
    private String carNo;   // 车牌号
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date toWorkStartTime; //上班发车时间
    private Integer toWorkStartLocation;     // 上班发车地点 -地址信息保存到地址表，保存地址id
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date afterWorkStartTime; //下班发车时间
    private Integer afterWorkStartLocation;     // 下班发车地点 -地址信息保存到地址表，保存地址id
    private Integer totalSeat; //总共可拼乘客数量
    @Transient
    private User user; //用户
    @Transient
    private Address toWorkStartLocationAddress;     // 上班发车地点对象
    @Transient
    private Address afterWorkStartLocationAddress;     // 下班发车地点对象
}

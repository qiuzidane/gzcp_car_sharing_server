package com.icbc.itsp.carsharing.enumType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum EmployeeLevel {
    //0-普通员工 ； 7-领导 ；9-管理员
    COMMON("0"),
    LEADER("7"),
    ADMIN("9");
    private String levle;
}

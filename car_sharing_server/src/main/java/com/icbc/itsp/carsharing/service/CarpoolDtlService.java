package com.icbc.itsp.carsharing.service;

import com.icbc.itsp.carsharing.entity.Carpool;
import com.icbc.itsp.carsharing.entity.CarpoolDtl;
import com.icbc.itsp.carsharing.entity.QueryParamVo;
import com.icbc.itsp.carsharing.enumType.CarpoolDtlStatus;
import com.icbc.itsp.carsharing.response.Result;
import java.util.List;

public interface CarpoolDtlService {
    List<CarpoolDtl> queryCarpoolDtl(CarpoolDtl carpoolDtl);

    Result addCarpoolDtl(CarpoolDtl carpoolDtl);

    Result deleteCarpoolDtl(Integer id, CarpoolDtlStatus newStatus);

    /**
     * 根据乘客id获取拼车总表记录
     * @param carpoolDtl
     * @return
     */
    List<Carpool> queryCarpoolByPassengerId(CarpoolDtl carpoolDtl);

    List<CarpoolDtl> queryCarpoolDtlByDriverOpenid(QueryParamVo queryParamVo);

    List<CarpoolDtl> queryCarpoolDtlByPassengerOpenid(QueryParamVo queryParamVo);

    int accessCarpoolDtl(CarpoolDtl carpoolDtl);
    int refuseCarpoolDtl(CarpoolDtl carpoolDtl);

    /**
     * 根据乘客openid查询待出行行程数量
     * @param queryParamVo
     * @return
     */
    Integer countStartOff(QueryParamVo<CarpoolDtl> queryParamVo);

    /**
     * 根据车主openid查询待审批拼车明细数量
     * @param queryParamVo
     * @return
     */
    Integer countPending(QueryParamVo<CarpoolDtl> queryParamVo);
}

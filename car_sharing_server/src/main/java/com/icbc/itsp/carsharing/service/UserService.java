package com.icbc.itsp.carsharing.service;

import com.icbc.itsp.carsharing.entity.QueryParamVo;
import com.icbc.itsp.carsharing.entity.User;
import com.icbc.itsp.carsharing.response.Result;
import java.util.List;

public interface UserService {
    /**
     * 查询用户
     * @param queryParamVo
     * @return
     */
    List<User> queryUser(QueryParamVo<User> queryParamVo);
    Result addUser(User user);
    Result updateUser(User user);
    Integer deleteUser(User user);
    User queryUserByUniqueKey(String openid);
    User queryUserByPrimaryKey(Integer id);
    Result getOpenid(String jsCode);
    Integer queryUserCount(QueryParamVo<User> queryParamVo);
}

package com.icbc.itsp.carsharing.service.serviceImpl;

import com.icbc.itsp.carsharing.dao.AddressDao;
import com.icbc.itsp.carsharing.dao.CommonDao;
import com.icbc.itsp.carsharing.dao.HistoryCarpoolDao;
import com.icbc.itsp.carsharing.dao.HistoryCarpoolDtlDao;
import com.icbc.itsp.carsharing.entity.*;
import com.icbc.itsp.carsharing.service.HistoryCarpoolService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class HistoryCarpoolServiceImpl implements HistoryCarpoolService {
    @Resource
    private HistoryCarpoolDao historyCarpoolDao;
    @Resource
    private CommonDao commonDao;
    @Resource
    private AddressDao addressDao;
    @Resource
    private HistoryCarpoolDtlDao historyCarpoolDtlDao;

    @Override
    public List<HistoryCarpool> queryHistoryCarpool(HistoryCarpool historyCarpool) {
        List<HistoryCarpool> list = historyCarpoolDao.queryHistoryCarpool(historyCarpool);
        for (HistoryCarpool historyCarpoolTemp : list){
            //拼车明细列表
            HistoryCarpoolDtl historyCarpoolDtl = new HistoryCarpoolDtl();
            historyCarpoolDtl.setCarpoolId(historyCarpoolTemp.getOrgCarpoolId());
            List<HistoryCarpoolDtl> historyCarpoolList = historyCarpoolDtlDao.queryHistoryCarpoolDtl(historyCarpoolDtl);
            historyCarpoolTemp.setHistoryCarpoolDtlList(historyCarpoolList);
            //发车地点对象
            if(historyCarpoolTemp.getStartLocation() != null){
                Address startAddress = addressDao.queryAddressByPrimaryKey(historyCarpoolTemp.getStartLocation());
                historyCarpoolTemp.setStartLocationAddress(startAddress);
            }
            if(historyCarpoolTemp.getStartLocation() != null){
                Address startAddress = addressDao.queryAddressByPrimaryKey(historyCarpoolTemp.getStartLocation());
                historyCarpoolTemp.setStartLocationAddress(startAddress);
            }
            if(historyCarpoolTemp.getEndLocation() != null){
                Address endAddress = addressDao.queryAddressByPrimaryKey(historyCarpoolTemp.getEndLocation());
                historyCarpoolTemp.setEndLocationAddress(endAddress);
            }
            if(historyCarpoolTemp.getDriverId() != null){
                User user = commonDao.queryUserByDriverId(historyCarpoolTemp.getDriverId());
                historyCarpoolTemp.setUser(user);
            }
            //设置已拼车人数
            Integer remainSeat = commonDao.getCarpoolRemainSeatHistory(historyCarpoolTemp.getOrgCarpoolId()) == null ? 0 : commonDao.getCarpoolRemainSeatHistory(historyCarpoolTemp.getOrgCarpoolId());
            Integer totalSeat = historyCarpoolTemp.getTotalSeat() == null ? 0 :historyCarpoolTemp.getTotalSeat();
            Integer carpoolSuccessSeat = totalSeat - remainSeat;
            historyCarpoolTemp.setCarpoolSuccessSeat(carpoolSuccessSeat > 0 ? carpoolSuccessSeat : 0);
        }
        return list;
    }
    @Override
    public List<HistoryCarpool> findHistoryCarpoolByIds(Map<String, Object> idList){
        List<HistoryCarpool> list = historyCarpoolDao.findHistoryCarpoolByIds(idList);
        return list;
    }
}

package com.icbc.itsp.carsharing.entity;

import lombok.Data;

/**
 * 用户信息
 */
@Data
public class User {
    private Integer id;
    private String openid; //微信用户id
    private String userName;  // 姓名
    private String weChatName; //微信名片
    private String gender; //性别
    private String department;   // 部门
    private String phoneNumber; //电话
    private String avatarUrl; //微信头像URL
    private String employeeFlag; //员工标志 ： 0-员工 ； 1-非员工 ；2-待审批
    private String employeeLevel; //员工级别 ： 0-普通员工 ； 7-领导 ；9-管理员
    private String auditFlag; //审核标志 ： 0-小程序审核前；1-小程序审核后
}

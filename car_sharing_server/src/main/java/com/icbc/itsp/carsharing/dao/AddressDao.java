package com.icbc.itsp.carsharing.dao;

import com.icbc.itsp.carsharing.entity.Address;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AddressDao {
    List<Address> queryAddress(Address address);
    Address queryAddressByPrimaryKey(@Param("id") Integer id);
    Integer insertAddress(Address address);
    Integer deleteByPrimaryKeySelective(Address address);
    Integer updateByUniqueKeySelective(Address address);
}

package com.icbc.itsp.carsharing.service.serviceImpl;
import com.icbc.itsp.carsharing.dao.AddressDao;
import com.icbc.itsp.carsharing.dao.DriverInfoDao;
import com.icbc.itsp.carsharing.dao.UserDao;
import com.icbc.itsp.carsharing.entity.DriverInfo;
import com.icbc.itsp.carsharing.entity.User;
import com.icbc.itsp.carsharing.service.DriverInfoService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

@Service
public class DriverInfoServiceImpl implements DriverInfoService {
    @Resource
    private DriverInfoDao driverReleaseDao;
    @Resource
    UserDao userDao;
    @Resource
    AddressDao addressDao;

    /**
     * 查询车主个人信息
     * @param openid
     * @return
     */
    @Override
    public DriverInfo queryDriverInfoByUniqueKey(String openid){
        return driverReleaseDao.queryDriverInfoByUniqueKey(openid);
    }

    /**
     * 查询车主个人信息
     * @param id
     * @return
     */
    @Override
    public DriverInfo queryDriverInfoByPrimaryKey(Integer id){
        return driverReleaseDao.queryDriverInfoByPrimaryKey(id);
    }

    /**
     * 查询所有车主信息
     * @return
     */
    @Override
    public List<DriverInfo> queryDriverInfo(DriverInfo driverInfoParam) {
        List<DriverInfo> list = driverReleaseDao.queryDriverInfo(driverInfoParam);
        for(DriverInfo driverInfo : list){
            User user = userDao.queryUserByUniqueKey(driverInfo.getOpenid());
            driverInfo.setUser(user);
            if(driverInfo.getToWorkStartLocation() != null){
                driverInfo.setToWorkStartLocationAddress(addressDao.queryAddressByPrimaryKey(driverInfo.getToWorkStartLocation()));
            }
            if(driverInfo.getAfterWorkStartLocation() != null){
                driverInfo.setAfterWorkStartLocationAddress(addressDao.queryAddressByPrimaryKey(driverInfo.getAfterWorkStartLocation()));
            }
        }
        return list;
    }

    /**
     * 新增车主信息
     * @param driverInfo
     * @return
     */
    @Override
    public boolean addDriverInfo(DriverInfo driverInfo) {
        return driverReleaseDao.insertDriverInfo(driverInfo);
    }

    /**
     * 更新车主信息
     * @param driverInfo
     * @return
     */
    @Override
    public boolean updateDriverInfo(DriverInfo driverInfo) {
        return driverReleaseDao.updateDriverInfo(driverInfo);
    }

}

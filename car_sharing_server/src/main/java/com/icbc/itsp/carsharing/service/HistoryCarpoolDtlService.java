package com.icbc.itsp.carsharing.service;

import com.icbc.itsp.carsharing.entity.HistoryCarpool;
import com.icbc.itsp.carsharing.entity.HistoryCarpoolDtl;
import java.util.List;

public interface HistoryCarpoolDtlService {
    /**
     * 查询拼车历史明细表接口
     * @param historyCarpoolDtl
     * @return
     */
    List<HistoryCarpoolDtl> queryHistoryCarpoolDtl(HistoryCarpoolDtl historyCarpoolDtl);

    /**
     * 根据乘客id获取拼车历史总表记录
     * @param historyCarpoolDtl
     * @return
     */
    List<HistoryCarpool> queryCarpoolByPassengerId(HistoryCarpoolDtl historyCarpoolDtl);
}

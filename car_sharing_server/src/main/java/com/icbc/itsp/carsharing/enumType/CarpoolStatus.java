package com.icbc.itsp.carsharing.enumType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum CarpoolStatus {
    /**
     * 1.司机发布拼车，初始状态为[拼车中]
     * 2.可拼座位数量小于等于0时，状态为[拼车完成]
     * 3.司机取消拼车总单，状态为[司机已取消]
     */
    UNFINISHED("拼车中"),
    FINISHED("拼车完成"),
    CANCEL("司机已取消");
    private String status;
}

package com.icbc.itsp.carsharing.dao;

import com.icbc.itsp.carsharing.entity.HistoryCarpool;

import java.util.List;
import java.util.Map;

public interface HistoryCarpoolDao {
	List<HistoryCarpool> queryHistoryCarpool(HistoryCarpool historyCarpool);
	void insert( HistoryCarpool historyCarpool);
	List<HistoryCarpool> findHistoryCarpoolByIds(Map<String, Object> idList);
	HistoryCarpool queryByOrgCarpoolId(Integer orgCarpoolId);
}

package com.icbc.itsp.carsharing.controller;

import com.icbc.itsp.carsharing.entity.HistoryCarpool;
import com.icbc.itsp.carsharing.response.Result;
import com.icbc.itsp.carsharing.service.HistoryCarpoolService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/historyCarpool")
@CrossOrigin
@Slf4j
public class HistoryCarpoolController {
    @Resource
    private HistoryCarpoolService historyCarpoolService;

    @RequestMapping(value = "/query",method = RequestMethod.GET)
    public Result queryHistoryCarpool( HistoryCarpool historyCarpool){
        try {
            List<HistoryCarpool> list = historyCarpoolService.queryHistoryCarpool(historyCarpool);
            return Result.success(list);
        }catch (Exception e){
            e.printStackTrace();
            return Result.fail("查询历史拼车总表记录发生异常！");
        }
    }
}

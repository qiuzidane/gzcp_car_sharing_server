package com.icbc.itsp.carsharing.controller;

import com.icbc.itsp.carsharing.entity.Address;
import com.icbc.itsp.carsharing.entity.Carpool;
import com.icbc.itsp.carsharing.entity.CarpoolDtl;
import com.icbc.itsp.carsharing.entity.QueryParamVo;
import com.icbc.itsp.carsharing.enumType.CarpoolDtlStatus;
import com.icbc.itsp.carsharing.response.Result;
import com.icbc.itsp.carsharing.service.AddressService;
import com.icbc.itsp.carsharing.service.CarpoolDtlService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/carpoolDtl")
@CrossOrigin
@Slf4j
public class CarpoolDtlController {

    @Resource
    private CarpoolDtlService carpoolDtlService;
    @Resource
    private AddressService addressService;

    /**
     * 查询拼车明细记录
     * @param carpoolDtl
     * @return
     */
    @RequestMapping(value = "/query",method = RequestMethod.GET)
    public Result queryCarpoolDtl( CarpoolDtl carpoolDtl){
        try {
            List<CarpoolDtl> carpoolDtlList = carpoolDtlService.queryCarpoolDtl(carpoolDtl);
            return Result.success(carpoolDtlList);
        }catch(Exception e){
            e.printStackTrace();
            return Result.fail("查询拼车明细异常");
        }
    }

    /**
     * 新增拼车明细记录
     * @param carpoolDtl
     * @return
     */
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public Result addCarpoolDtl(@RequestBody CarpoolDtl carpoolDtl){
        carpoolDtl.setStatus(CarpoolDtlStatus.PENDING.getStatus());
        try{
            if(carpoolDtl.getPassengerId() == null){
                return Result.fail("乘客id为空!");
            }
            if(carpoolDtl.getStartLocationAddress() != null){
                Address startLocationAddress = carpoolDtl.getStartLocationAddress();
                addressService.addAddress(startLocationAddress);
                carpoolDtl.setStartLocation(startLocationAddress.getId());
            }
            if(carpoolDtl.getEndLocationAddress() != null){
                Address endLocationAddress = carpoolDtl.getEndLocationAddress();
                addressService.addAddress(endLocationAddress);
                carpoolDtl.setEndLocation(endLocationAddress.getId());
            }
            return carpoolDtlService.addCarpoolDtl(carpoolDtl);
        } catch (Exception e){
            return Result.fail("新增拼车明细失败");
        }
    }

    /**
     * 取消拼车明细
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    public Result deleteCarpoolDtl(@RequestParam("id") Integer id){
        Result result = carpoolDtlService.deleteCarpoolDtl(id, CarpoolDtlStatus.CANCEL_BY_PASSENGER);
        return result;
    }

    /**
     * 根据乘客id获取拼车总表记录
     * @param carpoolDtl
     * @return
     */
    @RequestMapping(value = "/queryCarpoolByPassengerId",method = RequestMethod.GET)
    public Result queryCarpoolByPassengerId(CarpoolDtl carpoolDtl){
        List<Carpool> list = carpoolDtlService.queryCarpoolByPassengerId(carpoolDtl);
        return Result.success(list);
    }

    /**
     * 根据司机id及拼车状态获取拼车明细记录
     * @param driverOpenId 司机openid
     * @param carpoolStatusList 拼车状态
     */
    @RequestMapping(value = "/queryCarpoolDtlByOpenId",method = RequestMethod.GET)
    public Result queryCarpoolDtlByOpenId(@RequestParam("driverOpenId") String driverOpenId, @RequestParam(value = "carpoolStatusList",required = false) List<String> carpoolStatusList){
        try {
            QueryParamVo<CarpoolDtl> queryParamVo = new QueryParamVo<>();
            //参数查询对象
            Map<String,Object> map = new HashMap<>();
            map.put("driverOpenId",driverOpenId);
            map.put("carpoolStatusList",carpoolStatusList);
            queryParamVo.setOtherParam(map);
            List<CarpoolDtl> carpoolDtlList = carpoolDtlService.queryCarpoolDtlByDriverOpenid(queryParamVo);
            return Result.success(carpoolDtlList);
        }catch(Exception e){
            e.printStackTrace();
            return Result.fail("查询拼车明细异常");
        }
    }

    /**
     * 根据乘客id及拼车明细状态获取拼车明细记录
     * @param passengerOpenId
     * @param carpoolDtlStatus 拼车状态
     */
    @RequestMapping(value = "/queryCarpoolDtlByPassengerOpenId",method = RequestMethod.GET)
    public Result queryCarpoolDtlByPassengerOpenId(@RequestParam("passengerOpenId") String passengerOpenId,
                                                   @RequestParam(value = "carpoolDtlStatus",required = false) String carpoolDtlStatus){
        try {
            QueryParamVo<CarpoolDtl> queryParamVo = new QueryParamVo<>();
            //参数查询对象
            Map<String,Object> map = new HashMap<>();
            map.put("passengerOpenId", passengerOpenId);
            map.put("carpoolDtlStatus", carpoolDtlStatus);
            queryParamVo.setOtherParam(map);
            List<CarpoolDtl> carpoolDtlList = carpoolDtlService.queryCarpoolDtlByPassengerOpenid(queryParamVo);
            return Result.success(carpoolDtlList);
        }catch(Exception e){
            e.printStackTrace();
            return Result.fail("查询拼车明细异常");
        }
    }

    /**
     * 车主审批拼车明细
     * @param carpoolDtl---status  remark
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Result saveCarpoolDtl(@RequestBody CarpoolDtl carpoolDtl) {
        if(CarpoolDtlStatus.FINISHED.getStatus().equals(carpoolDtl.getStatus())){
            // 同意
            int i = carpoolDtlService.accessCarpoolDtl(carpoolDtl);
            if(i == 1) return Result.success();
            else if(i == 0) return Result.fail("没有剩余座位了。");
        }else if(CarpoolDtlStatus.FAILED.getStatus().equals(carpoolDtl.getStatus())){
            // 拒绝
            int i = carpoolDtlService.refuseCarpoolDtl(carpoolDtl);
            if(i == 1) return Result.success();
        }
        return Result.fail("审批出错，请重试。");
    }

    /**
     * 根据openid查询乘客待出行行程数量和车主待审批拼车明细数量
     * @param openid
     * @return
     */
    @RequestMapping(value = "/countStartOff",method = RequestMethod.GET)
    public Result countStartOff(@RequestParam("openid") String openid){
        try {
            QueryParamVo<CarpoolDtl> queryParamVo = new QueryParamVo<>();
            Map<String, Object> otherParam = new HashMap<>();
            otherParam.put("openid",openid);
            queryParamVo.setOtherParam(otherParam);
            Integer countStartOff = carpoolDtlService.countStartOff(queryParamVo);
            Integer countPending = carpoolDtlService.countPending(queryParamVo);
            Map<String,Object> countMap = new HashMap<>();
            countMap.put("countStartOff",countStartOff);
            countMap.put("countPending",countPending);
            return Result.success(countMap);
        }catch(Exception e){
            e.printStackTrace();
            return Result.fail("查询拼车明细异常");
        }
    }
}

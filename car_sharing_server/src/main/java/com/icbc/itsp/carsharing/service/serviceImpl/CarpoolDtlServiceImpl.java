package com.icbc.itsp.carsharing.service.serviceImpl;

import com.icbc.itsp.carsharing.dao.*;
import com.icbc.itsp.carsharing.entity.*;
import com.icbc.itsp.carsharing.enumType.CarpoolDtlStatus;
import com.icbc.itsp.carsharing.enumType.CarpoolStatus;
import com.icbc.itsp.carsharing.response.Result;
import com.icbc.itsp.carsharing.service.CarpoolDtlService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class CarpoolDtlServiceImpl implements CarpoolDtlService {

    @Resource
    private CommonDao commonDao;
    @Resource
    private CarpoolDtlDao carpoolDtlDao;
    @Resource
    private HistoryCarpoolDtlDao historyCarpoolDtlDao;
    @Resource
    private CarpoolDao carpoolDao;
    @Resource
    private AddressDao addressDao;
    @Resource
    private DriverInfoDao driverInfoDao;
    @Resource
    private UserDao userDao;

    @Override
    public List<CarpoolDtl> queryCarpoolDtl(CarpoolDtl carpoolDtl){
        List<CarpoolDtl> carpoolDtlList = carpoolDtlDao.queryCarpoolDtl(carpoolDtl);
        fillingCarpoolDtlList(carpoolDtlList);
        return carpoolDtlList;
    }

    @Override
    public Result addCarpoolDtl(CarpoolDtl carpoolDtl) {
        CarpoolDtl searchCarpoolDtl = new CarpoolDtl();
        searchCarpoolDtl.setPassengerId(carpoolDtl.getPassengerId());
        searchCarpoolDtl.setCarpoolId(carpoolDtl.getCarpoolId());
        List<CarpoolDtl> carpoolDtlList = carpoolDtlDao.queryCarpoolDtl(searchCarpoolDtl);
        if(carpoolDtlList != null && carpoolDtlList.size() > 0){
            return Result.fail("新增拼车明细失败：已经有此拼车的记录");
        }
        if(carpoolDao.queryByPrimaryKey(carpoolDtl.getCarpoolId()) == null){
            return Result.fail("查询该拼车信息失败，无法新增拼车明细");
        }
        if(carpoolDtl.getStartLocation() == null){
            return Result.fail("上车地点不能为空！");
        }
        if(commonDao.getCarpoolRemainSeat(carpoolDtl.getCarpoolId()) > 0){
            carpoolDtl.setStatus(CarpoolDtlStatus.PENDING.getStatus());
            carpoolDtl.setCreateTime(new Date());
            int addRows = carpoolDtlDao.addCarpoolDtl(carpoolDtl);
            if(addRows == 1){
                return Result.success();
            }else{
                return Result.fail("新增拼车明细失败");
            }
        } else {
            return Result.fail("抱歉，没有空余座位了，请选择其它拼车");
        }
    }

    @Override
    public Result deleteCarpoolDtl(Integer id, CarpoolDtlStatus newStatus) {
        try {
            CarpoolDtl carpoolDtl = carpoolDtlDao.queryByPrimary(id);
            if(carpoolDtl == null){
                return Result.fail("拼车明细记录不存在，取消拼车失败！");
            }
            String status = carpoolDtl.getStatus();
            //待车主审批状态或者拼车完成状态才能取消，其他状态不能取消
            if(!StringUtils.isEmpty(status) && (CarpoolDtlStatus.PENDING.getStatus().equals(status) || CarpoolDtlStatus.FINISHED.getStatus().equals(status))){
                //1. 拼车明细复制到历史表
                carpoolDtl.setStatus(newStatus.getStatus());
                HistoryCarpoolDtl historyCarpoolDtl = new HistoryCarpoolDtl();
                BeanUtils.copyProperties(carpoolDtl, historyCarpoolDtl);
                historyCarpoolDtl.setId(null);
                historyCarpoolDtlDao.insert(historyCarpoolDtl);
                //2. 删除拼车明细
                CarpoolDtl deleteCarpoolDtl = new CarpoolDtl();
                deleteCarpoolDtl.setId(carpoolDtl.getId());
                carpoolDtlDao.deleteCarpoolDtl(deleteCarpoolDtl);
            }else {
                return Result.fail("非待车主审批状态或者拼车完成状态，不能取消！");
            }

            //3. 检查拼车总的剩余可拼座位，如果大于0，根据拼车总表id更新状态
            int remainSeat = commonDao.getCarpoolRemainSeat(carpoolDtl.getCarpoolId());
            if(remainSeat > 0){
                Carpool carpool = new Carpool();
                carpool.setId(carpoolDtl.getCarpoolId());
                carpool.setStatus(CarpoolStatus.UNFINISHED.getStatus());
                carpoolDao.updateByPrimaryKeySelective(carpool);
            }
            return Result.success("取消拼车成功！");
        }catch (Exception e){
            e.printStackTrace();
            return Result.fail("取消拼车明细发生异常！");
        }
    }

    @Override
    public List<Carpool> queryCarpoolByPassengerId(CarpoolDtl carpoolDtl) {
        Map<String, Object> map = new HashMap<>();
        List<String> idList = carpoolDtlDao.findCarpoolIds(carpoolDtl);
        if(idList != null && idList.size() > 0){
            map.put("idList", idList);
            List<Carpool> carpoolList = carpoolDao.findCarpoolByIds(map);
            for(Carpool carpool: carpoolList){
                if(carpool.getStartLocation() != null){
                    Address startLocationAddress = addressDao.queryAddressByPrimaryKey(carpool.getStartLocation());
                    carpool.setStartLocationAddress(startLocationAddress);
                }
                if(carpool.getEndLocation() != null){
                    Address endLocationAddress = addressDao.queryAddressByPrimaryKey(carpool.getEndLocation());
                    carpool.setEndLocationAddress(endLocationAddress);
                }
                if(carpool.getDriverId() != null){
                    User user = commonDao.queryUserByDriverId(carpool.getDriverId());
                    carpool.setUser(user);
                }

            }
            return carpoolList;
        } else {
            return null;
        }
    }

    @Override
    public List<CarpoolDtl> queryCarpoolDtlByDriverOpenid(QueryParamVo queryParamVo) {
        List<CarpoolDtl> carpoolDtlList = carpoolDtlDao.queryCarpoolDtlByDriverOpenid(queryParamVo);
        fillingCarpoolDtlList(carpoolDtlList);
        return carpoolDtlList;
    }

    @Override
    public List<CarpoolDtl> queryCarpoolDtlByPassengerOpenid(QueryParamVo queryParamVo) {
        List<CarpoolDtl> carpoolDtlList = carpoolDtlDao.queryCarpoolDtlByPassengerOpenid(queryParamVo);
        fillingCarpoolDtlList(carpoolDtlList);
        return carpoolDtlList;
    }

    private void fillingCarpoolDtlList(List<CarpoolDtl> carpoolDtlList){
        for(CarpoolDtl carpoolDtlTemp : carpoolDtlList){
            if(carpoolDtlTemp == null){
                return;
            }
            Carpool carpool = carpoolDao.queryByPrimaryKey(carpoolDtlTemp.getCarpoolId());
            carpoolDtlTemp.setCarpool(carpool);
            if(carpool != null){
                DriverInfo driverInfo = driverInfoDao.queryDriverInfoByPrimaryKey(carpool.getDriverId());
                carpool.setDriverInfo(driverInfo);
                if(driverInfo != null){
                    User driverUser = userDao.queryUserByUniqueKey(driverInfo.getOpenid());
                    driverInfo.setUser(driverUser);
                }
            }

            if(carpool != null && carpool.getStartLocation() != null){
                Address carStartLocationAddress = addressDao.queryAddressByPrimaryKey(carpool.getStartLocation());
                carpoolDtlTemp.setCarStartLocationAddress(carStartLocationAddress);
            }
            if(carpoolDtlTemp.getStartLocation() != null){
                Address startLocationAddress = addressDao.queryAddressByPrimaryKey(carpoolDtlTemp.getStartLocation());
                carpoolDtlTemp.setStartLocationAddress(startLocationAddress);
            }
            if(carpoolDtlTemp.getEndLocation() != null){
                Address endLocationAddress = addressDao.queryAddressByPrimaryKey(carpoolDtlTemp.getEndLocation());
                carpoolDtlTemp.setEndLocationAddress(endLocationAddress);
            }
            if(carpoolDtlTemp.getPassengerId() != null){
                User user = commonDao.queryUserByPassengerId(carpoolDtlTemp.getPassengerId());
                carpoolDtlTemp.setUser(user);
                /*if(user != null){
                    carpoolDtlTemp.setAvatarUrl(user.getAvatarUrl());
                    carpoolDtlTemp.setPassengerDepartment(user.getDepartment());
                    carpoolDtlTemp.setUserName(user.getUserName());
                    carpoolDtlTemp.setWeChatName(user.getWeChatName());
                }*/
            }
        }
    }

    /**
     * 审批拼车-同意
     * 0 无座位
     * 1 成功
     * -1 异常
     */
    @Override
    public int accessCarpoolDtl(CarpoolDtl carpoolDtl) {
        try{
            // 根据总单ID查找已拼车人数，判断是否剩余座位数
            int remainSeat = commonDao.getCarpoolRemainSeat(carpoolDtl.getCarpoolId());
            if(remainSeat < 1) return 0;
            // 更新拼车明细表状态
            CarpoolDtl carpoolDtlParam = new CarpoolDtl();
            carpoolDtlParam.setStatus(CarpoolDtlStatus.FINISHED.getStatus());
            carpoolDtlParam.setId(carpoolDtl.getId());
            carpoolDtlDao.updateCarpoolDtl(carpoolDtlParam);
            if(remainSeat == 1){
                // 如果已满员，其他未同意的拼车取消
                CarpoolDtl carpoolDtlTmep = new CarpoolDtl();
                carpoolDtlTmep.setCarpoolId(carpoolDtl.getCarpoolId());
                carpoolDtlTmep.setStatus(CarpoolDtlStatus.FAILED.getStatus());
                carpoolDtlTmep.setRemark("可座位为0，拼车失败");
                carpoolDtlDao.updateFailedCarpool(carpoolDtlTmep);
                // 总单状态修改
                Carpool carpool = new Carpool();
                carpool.setId(carpoolDtl.getCarpoolId());
                carpool.setStatus(CarpoolStatus.FINISHED.getStatus());
                carpoolDao.updateByPrimaryKeySelective(carpool);
            }
            return 1;
        }catch (Exception e) {
            log.error("审批异常。", e);
        }
        return -1;
    }

    /**
     * 审批拼车-拒绝
     * 1成功
     * -1 异常
     * @return
     */
    @Override
    public int refuseCarpoolDtl(CarpoolDtl carpoolDtl){
        try{
            // 更新拼车明细表状态
            CarpoolDtl carpoolDtlParam = new CarpoolDtl();
            carpoolDtlParam.setStatus(CarpoolDtlStatus.FAILED.getStatus());
            carpoolDtlParam.setId(carpoolDtl.getId());
            carpoolDtlDao.updateCarpoolDtl(carpoolDtl);
            return 1;
        }catch (Exception e) {
            log.error("审批异常。", e);
        }
        return -1;
    }

    @Override
    public Integer countStartOff(QueryParamVo<CarpoolDtl> queryParamVo){
        Integer i = carpoolDtlDao.countStartOff(queryParamVo);
        return i;
    }

    @Override
    public Integer countPending(QueryParamVo<CarpoolDtl> queryParamVo){
        Integer i = carpoolDtlDao.countPending(queryParamVo);
        return i;
    }
}

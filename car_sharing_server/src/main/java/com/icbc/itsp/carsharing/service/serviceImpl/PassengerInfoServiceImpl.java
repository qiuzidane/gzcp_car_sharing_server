package com.icbc.itsp.carsharing.service.serviceImpl;

import com.icbc.itsp.carsharing.dao.AddressDao;
import com.icbc.itsp.carsharing.dao.PassengerInfoDao;
import com.icbc.itsp.carsharing.dao.UserDao;
import com.icbc.itsp.carsharing.entity.PassengerInfo;
import com.icbc.itsp.carsharing.entity.User;
import com.icbc.itsp.carsharing.service.PassengerInfoService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

@Service
public class PassengerInfoServiceImpl implements PassengerInfoService {
    @Resource
    private PassengerInfoDao passengerInfoDao;
    @Resource
    private UserDao userDao;
    @Resource
    private AddressDao addressDao;

    @Override
    public PassengerInfo queryPassengerInfoByUniqueKey(String openid) {
        return passengerInfoDao.queryPassengerInfoByUniqueKey(openid);
    }
    @Override
    public List<PassengerInfo> queryPassengerInfo(PassengerInfo passengerInfoParam) {
        List<PassengerInfo> list = passengerInfoDao.queryPassengerInfo(passengerInfoParam);
        for(PassengerInfo passengerInfo : list){
            User user = userDao.queryUserByUniqueKey(passengerInfo.getOpenid());
            if(passengerInfo.getStartLocation() != null){
                passengerInfo.setStartLocationAddress(addressDao.queryAddressByPrimaryKey(passengerInfo.getStartLocation()));
            }
            if(passengerInfo.getEndLocation() != null){
                passengerInfo.setEndLocationAddress(addressDao.queryAddressByPrimaryKey(passengerInfo.getEndLocation()));
            }
            passengerInfo.setUser(user);
        }
        return list;
    }

    /**
     * 新增
     * @param passengerInfo
     * @return
     */
    @Override
    public boolean addPassengerInfo(PassengerInfo passengerInfo) {
        return passengerInfoDao.insertPassengerInfo(passengerInfo);
    }

    /**
     * 更新
     * @param passengerInfo
     * @return
     */
    @Override
    public boolean updatePassengerInfo(PassengerInfo passengerInfo) {
        try {
            return passengerInfoDao.updatePassengerInfo(passengerInfo);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}

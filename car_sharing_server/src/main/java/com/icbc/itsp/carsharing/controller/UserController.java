package com.icbc.itsp.carsharing.controller;

import com.icbc.itsp.carsharing.entity.QueryParamVo;
import com.icbc.itsp.carsharing.entity.User;
import com.icbc.itsp.carsharing.enumType.EmployeeFlag;
import com.icbc.itsp.carsharing.response.Page;
import com.icbc.itsp.carsharing.response.Result;
import com.icbc.itsp.carsharing.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
@CrossOrigin
@Slf4j
public class UserController {
    @Resource
    private UserService userService;

    /**
     * 根据openid查询单个用户
     * @param userParam
     * @return
     */
    @RequestMapping(value = "/query",method = RequestMethod.GET)
    public Result queryUser(User userParam){
        try {
            if(userParam == null || StringUtils.isEmpty(userParam.getOpenid())){
                return Result.fail("openid为空");
            }
            User user = userService.queryUserByUniqueKey(userParam.getOpenid());
            return Result.success(user);
        }catch(Exception e){
            e.printStackTrace();
            log.error("查询用户信息发生异常:",e);
            return Result.fail("查询用户信息发生异常！");
        }
    }

    /**
     * 分页查询所有用户
     * @param paramobj
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/queryByPage",method = RequestMethod.GET)
    public Page<User> queryByPage(User paramobj, @RequestParam(value = "curPage") int pageNo,
                                      @RequestParam(value = "pageSize") int pageSize){
        Page<User> page = Page.newInstance();
        try {
            QueryParamVo<User> queryParamVo = new QueryParamVo();
            //设置分页参数
            queryParamVo.setPageSize(pageSize);
            queryParamVo.setPageNo(pageNo);
            //模糊查询对象
            User likeObj = new User();
            likeObj.setUserName(paramobj.getUserName());
            likeObj.setWeChatName(paramobj.getWeChatName());
            queryParamVo.setLikeobj(likeObj);
            //参数查询对象
            paramobj.setUserName(null);
            paramobj.setWeChatName(null);
            queryParamVo.setParamobj(paramobj);
            List<User> list = userService.queryUser(queryParamVo);
            //统计所有用户(过滤掉未填写部门和用户名的user）
            QueryParamVo<User> queryParamVoCountAll = new QueryParamVo<>();
            User countAllUser = new User();
            queryParamVoCountAll.setParamobj(countAllUser);
            Integer totalCount = userService.queryUserCount(queryParamVoCountAll);

            //查询待审批用户数量(过滤掉未填写部门和用户名的user）
            QueryParamVo<User> queryParamVoPending = new QueryParamVo<>();
            User countPendingUser = new User();
            countPendingUser.setEmployeeFlag(EmployeeFlag.PENDING.getFlag());
            queryParamVoPending.setParamobj(countPendingUser);
            Integer pendingCount = userService.queryUserCount(queryParamVoPending);
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("pendingCount", pendingCount);
            resultMap.put("totalCount", totalCount);
            page.setParams(resultMap);

            page.setPageNo(pageNo);
            page.setPageSize(pageSize);
            page.setTotalCount(totalCount);
            page.setResults(list);
            return page;
        }catch(Exception e){
            e.printStackTrace();
            log.error("查询用户信息发生异常:",e);
            return page;
        }
    }

    /**
     * 新增用户
     * @param user
     * @return
     */
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public Result addUser(@RequestBody User user){
        try {
            Result result = userService.addUser(user);
            return result;
        }catch(Exception e){
            e.printStackTrace();
            log.error("添加用户信息发生异常:",e);
            return Result.fail("添加用户信息发生异常！");
        }
    }

    /**
     * 修改用户
     * @param user
     * @return
     */
    @RequestMapping(value = "/update",method = RequestMethod.POST)
    public Result updateUser(@RequestBody User user){
        try {
            Result result  = userService.updateUser(user);
            return result;
        }catch(Exception e){
            e.printStackTrace();
            log.error("修改用户信息发生异常:",e);
            return Result.fail("修改用户信息发生异常！");
        }
    }

    /**
     * 删除用户
     * @param user
     * @return
     */
    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    public Result deleteUser(User user){
        try {
            Integer i = userService.deleteUser(user);
            if(i > 0){
                return Result.success("删除用户成功！");
            }else {
                return Result.fail("删除用户失败！");
            }
        }catch(Exception e){
            e.printStackTrace();
            log.error("删除用户信息发生异常:",e);
            return Result.fail("删除用户信息发生异常！");
        }
    }

    /**
     * 获取openid
     * @return
     */
    @RequestMapping(value = "/getOpenid", method = RequestMethod.GET)
    public Object getOpenid(@RequestParam("jsCode") String jsCode){
        Result result = userService.getOpenid(jsCode);
        return result;
    }
}

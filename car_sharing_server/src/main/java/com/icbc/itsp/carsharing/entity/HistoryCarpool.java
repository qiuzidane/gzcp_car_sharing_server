package com.icbc.itsp.carsharing.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * 拼车历史总表
 */
@Data
public class HistoryCarpool {
    private Integer id;
    private Integer orgCarpoolId; //原总单id
    private Integer driverId;//车主个人信息表id
    private String carNo;   // 车牌号
    private String driverName;  // 司机姓名
    private String phoneNumber;   // 司机电话
    private Integer startLocation;     // 发车地点 -地址信息保存到地址表，保存地址id
    private Integer endLocation;     // 终点 -地址信息保存到地址表，保存地址id
    private String line; //线路描述
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime; //发车时间
    private Integer totalSeat; //总共可拼乘客数量
    private String status; //状态
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime; //发布时间

    @Transient
    private Address startLocationAddress; //起点地址对象
    @Transient
    private Address endLocationAddress; //终点地址对象
    @Transient
    private User user;
    @Transient
    private List<HistoryCarpoolDtl> historyCarpoolDtlList; //历史明细对象列表

    @Transient
    private Integer carpoolSuccessSeat; //已拼车人数
}

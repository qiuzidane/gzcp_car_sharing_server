package com.icbc.itsp.carsharing.dao;

import com.icbc.itsp.carsharing.entity.PassengerInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PassengerInfoDao {
    /**
     * 查询个人乘客信息
     * @return
     */
    PassengerInfo queryPassengerInfoByUniqueKey(@Param("openid")String openid);

    /**
     * 查询所有信息
     * @return
     */
    List<PassengerInfo> queryPassengerInfo(PassengerInfo passengerInfo);

    /**
     * 乘客新增个人信息
     * @param passengerInfo
     * @return
     */
    boolean insertPassengerInfo(PassengerInfo passengerInfo);

    /**
     * 乘客更新个人信息
     * @param passengerInfo
     * @return
     */
    boolean updatePassengerInfo(PassengerInfo passengerInfo);
}

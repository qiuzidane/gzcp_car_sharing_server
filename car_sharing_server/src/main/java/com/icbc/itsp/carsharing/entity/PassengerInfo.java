package com.icbc.itsp.carsharing.entity;

import lombok.Data;

import javax.persistence.Transient;


/**
 * 乘客信息
 */
@Data
public class PassengerInfo {
    private Integer id;
    private String openid;  //微信用户id
    private Integer startLocation; //乘客上车地点 -地址信息保存到地址表，保存地址id
    private Integer endLocation; //乘客下车地点 -地址信息保存到地址表，保存地址id
    @Transient
    private User user; //用户
    @Transient
    private Address startLocationAddress; //开始地址
    @Transient
    private Address endLocationAddress; //结束地址
}

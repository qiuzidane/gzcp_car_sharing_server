package com.icbc.itsp.carsharing.controller;

import com.icbc.itsp.carsharing.entity.Address;
import com.icbc.itsp.carsharing.response.Result;
import com.icbc.itsp.carsharing.service.AddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/address")
@CrossOrigin
@Slf4j
public class AddressController {
    @Resource
    private AddressService addressService;

    /**
     * 查询地址
     * @param address
     * @return
     */
    @RequestMapping(value = "/query",method = RequestMethod.GET)
    public Result queryAddress(Address address){
        try {
            List<Address> list = addressService.queryAddress(address);
            return Result.success(list);
        }catch(Exception e){
            e.printStackTrace();
            log.error("查询地址信息发生异常:",e);
            return Result.fail("查询地址信息发生异常！");
        }
    }

    /**
     * 新增地址
     * @param address
     * @return
     */
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public Result addAddress(@RequestBody Address address){
        try {
            Result result = addressService.addAddress(address);
            return result;
        }catch(Exception e){
            e.printStackTrace();
            log.error("添加地址信息发生异常:",e);
            return Result.fail("添加地址信息发生异常！");
        }
    }

    /**
     * 修改地址
     * @param address
     * @return
     */
    @RequestMapping(value = "/update",method = RequestMethod.POST)
    public Result updateAddress(@RequestBody Address address){
        try {
            if(address == null || address.getId() == null){
                return Result.fail("无更新内容，地址更新失败!");
            }
            Result result  = addressService.updateAddress(address);
            return result;
        }catch(Exception e){
            e.printStackTrace();
            log.error("修改地址信息发生异常:",e);
            return Result.fail("修改地址信息发生异常！");
        }
    }


    /**
     * 删除地址
     * @param address
     * @return
     */
    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    public Result deleteAddress(Address address){
        try {
            Integer i = addressService.deleteAddress(address);
            if(i > 0){
                return Result.success("删除地址成功！");
            }else {
                return Result.fail("删除地址失败！");
            }
        }catch(Exception e){
            e.printStackTrace();
            log.error("删除地址信息发生异常:",e);
            return Result.fail("删除地址信息发生异常！");
        }
    }
}
